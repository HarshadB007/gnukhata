package gnukhata.controllers;

import gnukhata.globals;
import gnukhata.controllers.reportmodels.AddVoucher;
import gnukhata.controllers.reportmodels.VoucherDetail;
import gnukhata.views.EditVoucherComposite;
import gnukhata.views.ViewVoucherDetailsComposite;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.apache.xmlrpc.XmlRpcException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

public class transactionController {
	public static String[] getContra()
	{
		try {
			Object[] contraAccounts =(Object[]) globals.client.execute("getaccountsbyrule.getContraAccounts",new Object[]{globals.session[0]});
			String[] accountList = new String[contraAccounts.length];
			for(int i =0; i<contraAccounts.length; i++)
			{
				accountList[i] = contraAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{}; 
		}
				
	}
	public static String[] getJournal()
	{
		try {
			Object[] journalAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getJournalAccounts",new Object[]{globals.session[0]});
			String[] accountList = new String[journalAccounts.length];
			for(int i =0;i <journalAccounts.length;i++)
			{
				accountList[i] = journalAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getPayment(String DrCrFlag)
	{	
		
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		
		
		try {
			Object[] paymentAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getPaymentAccounts",serverParams);
			String[] accountList = new String[paymentAccounts.length];
			for(int i =0;i <paymentAccounts.length;i++)
			{
				accountList[i] = paymentAccounts[i].toString();
				System.out.println(paymentAccounts[i]);
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getReceipt(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] receiptAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getReceivableAccounts",serverParams);
			String[] accountList = new String[receiptAccounts.length];
			for(int i =0;i <receiptAccounts.length;i++)
			{
				accountList[i] = receiptAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getCreditNote(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		try {
			Object[] creditNoteAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getCreditNoteAccounts",serverParams);
			String[] accountList = new String[creditNoteAccounts.length];
			for(int i =0;i <creditNoteAccounts.length;i++)
			{
				accountList[i] = creditNoteAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getDebitNote(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] DebitNoteAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getDebitNoteAccounts",serverParams);
			String[] accountList = new String[DebitNoteAccounts.length];
			for(int i =0;i <DebitNoteAccounts.length;i++)
			{
				accountList[i] = DebitNoteAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getSales(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] salesAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getSalesAccounts",serverParams);
			String[] accountList = new String[salesAccounts.length];
			for(int i =0;i <salesAccounts.length;i++)
			{
				accountList[i] = salesAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getSalesReturn(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);

		try {
			Object[] salesReturnAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getSalesReturnAccounts",serverParams);
			String[] accountList = new String[salesReturnAccounts.length];
			for(int i =0;i <salesReturnAccounts.length;i++)
			{
				accountList[i] = salesReturnAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getPurchase(String DrCrFlag)
	{	
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		try {
			Object[] purchaseAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getPurchaseAccounts",serverParams);
			String[] accountList = new String[purchaseAccounts.length];
			for(int i =0;i <purchaseAccounts.length;i++)
			{
				accountList[i] = purchaseAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	public static String[] getPurchaseReturn(String DrCrFlag)
	{		
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{DrCrFlag});
		serverParams.add(globals.session[0]);
		try {
			Object[] purchaseReturnAccounts = (Object[]) globals.client.execute("getaccountsbyrule.getPurchaseReturnAccounts",serverParams);
			String[] accountList = new String[purchaseReturnAccounts.length];
			for(int i =0;i <purchaseReturnAccounts.length;i++)
			{
				accountList[i] = purchaseReturnAccounts[i].toString();
			}
			return accountList;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return new String[]{};
		}
	}
	
	public static boolean setTransaction(List<String> masterQueryParams, List<Object> detailQueryParams)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(masterQueryParams);
		serverParams.add(detailQueryParams);
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("transaction.setTransaction",serverParams);
			return true;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.getMessage();
			return false;
		}
		
	}
	
	public static boolean editTransaction(List<String> masterQueryParams, List<Object> detailQueryParams )
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(masterQueryParams);
		serverParams.add(detailQueryParams);
		serverParams.add(globals.session[0]);
			try {
				Object success = globals.client.execute("transaction.editVoucher", serverParams);
				return true;
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		
		
	}
	
	public static ArrayList<VoucherDetail> searchVouchers(int searchFlag, String txtvoucherno, String startRange, String endRange, String txtnarration, double amount)
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{searchFlag, txtvoucherno, startRange, endRange, txtnarration, amount});
		serverParams.add(globals.session[0]);
		try {
			Object[] result = (Object[]) globals.client.execute("transaction.searchVoucher",serverParams);
			ArrayList<VoucherDetail> Vouchers = new ArrayList<VoucherDetail>();
			for(int r =0; r < result.length; r ++)
			{
				Object[] voucher = (Object[]) result[r];
				String voucherCode = voucher[0].toString();
				String voucherNo = voucher[1].toString();
				String dateOfTransaction = voucher[2].toString();
				String voucherType = voucher[3].toString();
				String narration = "(" +voucher[7].toString()+")";
				narration=WordUtils.wrap(narration, 50);
				String drAccount = voucher[4].toString();
				if(!narration.trim().equals("()"))
				{
					drAccount= drAccount+"\n"+ narration ;
				}
				String crAccount = voucher[5].toString();
				String voucheramount = voucher[6].toString();
				
				
				int lockflag = (int) voucher[8];
				//serverParams.remove(0);
				//serverParams.add(0, new Object[]{voucherCode} );
				Object[] VoucherMaster = getVoucherMaster(Integer.valueOf(voucherCode ));
				String projectName = VoucherMaster[4].toString();
				
				//String projectName = voucher[8].toString();
				
				VoucherDetail vd = new VoucherDetail(voucherNo, voucherType, dateOfTransaction, drAccount, crAccount, voucheramount, narration,  voucherCode,projectName, lockflag );
				Vouchers.add(vd);
			}
			return Vouchers;
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ArrayList<VoucherDetail> emptydetails = new ArrayList<VoucherDetail>();
			return emptydetails;
		}
		
		
		
	}
	public static Boolean setLockFlag(int vouchercode, int lockflag)
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{vouchercode,lockflag});
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("transaction.voucherLock",serverParams);
			Boolean lock = Boolean.valueOf(result.toString() );
			return lock;
			
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static int getLockFlag(int vouchercode)
	{
		ArrayList<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{vouchercode});
		serverParams.add(globals.session[0]);
		try {
			Object result = globals.client.execute("transaction.getvoucherLockFlag",serverParams);
			int lock = (int) result;
			System.out.println("this is the lock:"+ lock+"this is vouchercode:"+vouchercode);
			return lock;
			
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 1;
		}
	}
	
	
	
	public static void showVoucherDetail(Composite grandParent, String typeFlag,int voucherCode,boolean findvoucher, boolean tbdrilldown, boolean psdrilldown,String tbType, boolean ledgerDrilldown,String startDate,String oldfromdate, String endDate, String oldenddate,String AccountName,String oldaccname, String ProjectName,String oldprojectname,boolean narrationFlag,boolean oldnarration,String selectproject,String oldselectproject,boolean dualledgerflag, int lock,int searchflag, String[] searchDetails)
	{
		ViewVoucherDetailsComposite vvdc = new ViewVoucherDetailsComposite(grandParent, SWT.NONE, typeFlag, voucherCode, false, tbdrilldown, psdrilldown, tbType, ledgerDrilldown, startDate,oldfromdate, endDate,oldenddate, AccountName,oldaccname, ProjectName,oldprojectname, narrationFlag,oldnarration, selectproject,oldselectproject,dualledgerflag,lock, searchflag,searchDetails);
		vvdc.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height );
	}
public static void showVoucherDetail(Composite grandParent,String typeFlag,int voucherCode,boolean findvoucher, int lock, int searchflag, String[] searchDetails)
{
	ViewVoucherDetailsComposite vvdc = new ViewVoucherDetailsComposite(grandParent, SWT.NONE, typeFlag, voucherCode, findvoucher, false, false, "", false, "","", "","", "","","", "", false,false, "","",false, lock,searchflag,searchDetails);
	vvdc.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height );
	
}
public static Object[] getVoucherMaster(int voucherCode)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	serverParams.add(new Object[]{voucherCode} );
	serverParams.add(globals.session[0] );
	try {
		Object[] result = (Object[])globals.client.execute("transaction.getVoucherMaster", serverParams);
		return result;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return new Object[]{};
	}
}
public static ArrayList<AddVoucher> getVoucherDetails(int voucherCode)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	serverParams.add(new Object[]{voucherCode} );
	serverParams.add(globals.session[0] );
	try {
		Object[] result = (Object[])globals.client.execute("transaction.getVoucherDetails", serverParams);
		ArrayList<AddVoucher> voucherDetails = new ArrayList<AddVoucher>(); 
		for(int row = 0; row < result.length; row ++)
		{
			Object[] TransactionRow = (Object[]) result[row];
			String drCr = TransactionRow[1].toString();
			String accountName = TransactionRow[0].toString();
			double drAmount = 0.00;
					double crAmount = 0.00;
					if(drCr.equals("Dr"))
					{
						drAmount = Double.parseDouble(TransactionRow[2].toString() );
					}
					if(drCr.equals("Cr"))
					{
						crAmount = Double.parseDouble(TransactionRow[2].toString() );
					}
			AddVoucher vr = new AddVoucher(drCr, accountName, drAmount, crAmount);
			voucherDetails.add(vr);
		}
		return voucherDetails; 
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return new ArrayList<AddVoucher>();
	}

}
public static void showEditCloneVoucher(Composite grandparent, int style,String voucherType, int voucherCode,boolean findvoucher, int editFlag, boolean tbdrilldown,boolean psdrilldown,String tbType, boolean ledgerDrilldown, String startDate ,String oldfromdate, String endDate,String oldenddate, String AccountName,String oldaccname, String ProjectName,String oldprojectname, boolean narrationFlag,boolean oldnarration,String selectproject,String oldselectproject,boolean dualledgerflag, int searchFlag, String[] searchDetails )
{
	
	EditVoucherComposite evc = new EditVoucherComposite(grandparent, SWT.NONE, voucherType, voucherCode,findvoucher, editFlag, tbdrilldown, psdrilldown, tbType, ledgerDrilldown, startDate,oldfromdate, endDate,oldenddate, AccountName,oldaccname, ProjectName,oldprojectname, narrationFlag,oldnarration, selectproject,oldselectproject,dualledgerflag,searchFlag,searchDetails);
	evc.setSize(grandparent.getClientArea().width, grandparent.getClientArea().height );
}

public static boolean deleteVoucher(int voucherCode)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	try {
		serverParams.add(new Object[]{voucherCode});
		serverParams.add(globals.session[0]);
		
		Object success = globals.client.execute("transaction.deleteVoucher", serverParams);
		return true;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}
	
}


public static String getLastDate(String typeFlag)
{
	ArrayList<Object> serverParams = new ArrayList<Object>();
	String fromDateParam = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
	
	serverParams.add(new Object[]{fromDateParam,typeFlag});
	serverParams.add(globals.session[0]);
	try {
		Object lastDate = globals.client.execute("transaction.getLastReffDate", serverParams);
		return lastDate.toString();
	
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return "";
	}
	
}

public static String getLastReference(String typeFlag)
{
	ArrayList<Object> serverParams=new ArrayList<Object>();
	serverParams.add(new Object[]{typeFlag});
	serverParams.add(globals.session[0]);

	try {
		Object voucherno = globals.client.execute("transaction.getLastReference",serverParams);
		return voucherno.toString();
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return "";
	}
	
	
	}

public static String[] getAllProjects()
{
	try {
		Object[] projects = (Object[]) globals.client.execute("organisation.getAllProjects", new Object[]{globals.session[0]});
		String[] allProjects = new String[projects.length];
		for(int i = 0; i < projects.length; i++ )
		{
			Object[] projectRow = (Object[]) projects[i];
			allProjects[i] = projectRow[1].toString();
		}
		return allProjects;
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return new String[]{};
	}
	
}
}


