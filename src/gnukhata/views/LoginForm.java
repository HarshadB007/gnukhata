package gnukhata.views;

/*
 * @authors
 * Amit Chougule <acamit333@gmail.com>,
 * Girish Joshi <girish946@gmail.com>, 
 */




import gnukhata.globals;
import gnukhata.controllers.StartupController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
/*
 * this class is the loginform for the gnukhata.
 */
public class LoginForm extends Shell
{
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;

	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	Label lblRegiNo;
	Label lblUserName;
	Text txtUserName;
	Label lblPassword;
	Text txtPassword;
	Button btnLogin;
	Button btnBack;
	Button btnForgetPassword;
	String username;
	String question;
	String Username;
	public LoginForm() {
		super(Display.getDefault());
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		this.setText("Login Form");
		
		Label lblWelcome = new Label(this,SWT.None);
		lblWelcome.setText("Welcome");
		lblWelcome.setFont(new Font(display, "Times New Roman", 14, SWT.BOLD));
		FormData layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(5);
		lblWelcome.setLayoutData(layout);
		
		Label lblHeadline = new Label(this,SWT.None);
		lblHeadline.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
		lblHeadline.setText("GNUKhata: A Free and Open Source Accounting Software");
		layout = new FormData();
		layout.top = new FormAttachment(lblWelcome,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(51);
		layout.bottom = new FormAttachment(8);
		lblHeadline.setLayoutData(layout);
		
		Label lblLogo = new Label(this, SWT.None);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(12);
		lblLogo.setLayoutData(layout);
		
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD) );
		lblOrgDetails.setText(strOrgName.replace("&", "&&")+"\n"+"For Financial Year "+"From "+strFromYear+" To "+strToYear );
		layout = new FormData();
		layout.top = new FormAttachment(10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(69);
		layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(20);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(26);
		lblLine.setLayoutData(layout);
		
		
		Label lblheader = new Label(this,SWT.NONE);
		lblheader.setFont( new Font(display,"Times New Roman", 18, SWT.BOLD) );
		lblheader.setText("Login");
		layout = new FormData();
		layout.top = new FormAttachment(lblLine, 5);
		layout.left = new FormAttachment(46);
		layout.right = new FormAttachment(69);
		layout.bottom = new FormAttachment(32);
		lblheader.setLayoutData(layout);
		
		lblUserName = new Label(this, SWT.NONE);
		lblUserName.setText("&User Name :");
		lblUserName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(46);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(49);
		lblUserName.setLayoutData(layout);
		
		txtUserName = new Text(this, SWT.BORDER);
		txtUserName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		txtUserName.setText("admin");
		txtUserName.selectAll();
		layout = new FormData();
		layout.top = new FormAttachment(45);
		layout.left = new FormAttachment(45);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(49);
		txtUserName.setLayoutData(layout);
		
		
		lblPassword = new Label(this, SWT.NONE);
		lblPassword.setText("&Password :");
		lblPassword.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(54);
		lblPassword.setLayoutData(layout);
		
		txtPassword = new Text(this, SWT.BORDER);
		txtPassword.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		txtPassword.setEchoChar('*');
		txtPassword.setMessage("admin");
		layout = new FormData();
		layout.top = new FormAttachment(50);
		layout.left = new FormAttachment(45);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(54);
		txtPassword.setLayoutData(layout);
		
		btnLogin = new Button(this,SWT.PUSH);
		btnLogin.setText("&Login");
		btnLogin.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(45);
		//layout.bottom = new FormAttachment(65);
		btnLogin.setLayoutData(layout);
		
		btnBack = new Button(this,SWT.PUSH);
		btnBack.setText("&Change Organisation");
		btnBack.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(btnLogin,10);
		//layout.right = new FormAttachment(52);
		//layout.bottom = new FormAttachment(65);
		btnBack.setLayoutData(layout);
		
		btnForgetPassword = new Button(this,SWT.PUSH);
		btnForgetPassword.setText("&Forget Password");
		btnForgetPassword.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(btnBack,10);
		//layout.right = new FormAttachment(52);
		//layout.bottom = new FormAttachment(65);
		btnForgetPassword.setLayoutData(layout);
		//btnForgetPassword.setEnabled(false);
		
		
		this.setImage(globals.icon);
		this.setBackgroundImage(globals.backImg);
		this.setBackgroundMode(SWT.INHERIT_FORCE);
		this.getAccessible();
		this.setEvents();
		this.pack();
		this.open();
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);

		globals.setThemeColor(this, Background, Foreground);
	    globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

		this.showView();
		}
	private void setEvents()
	{
	
		txtPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR | e.keyCode==SWT.KEYPAD_CR)
				{	
						btnLogin.notifyListeners(SWT.Selection, new Event());
						
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					
					//txtUserName.selectAll();
					txtUserName.setFocus();
					//btnForgetPassword.setEnabled(false);
				}
			}
		});
		
		
		btnLogin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				if (txtUserName.getText().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please enter a User name");
					alert.open();
					txtUserName.setFocus();
					//btnForgetPassword.setEnabled(false);
					return;
				}
				if(txtPassword.getText().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please enter a password");
					alert.open();
					//txtPassword.setFocus();
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
						
							txtPassword.setFocus();
							
						}
					});
				
					return;
				}
				
				if (StartupController.login(txtUserName.getText(),txtPassword.getText()))
				{
					/*MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("Login Successful");
					msg.open();*/
					dispose();
					StartupController.showMainShell(display, 2);
				}
				else
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
		            msg.setText("Error!");
					
					msg.setMessage("Please enter valid Username and Password");
					msg.open();
					txtUserName.selectAll();
					txtUserName.setFocus();
					
					
				}
				}
				
		});
		
		
		txtUserName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					
					txtPassword.setFocus();
					return;
						
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtUserName.setFocus();	
					return;
				}
			}
		});
		
		btnLogin.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0)
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					btnBack.setFocus();
				}
			}
		});
		
		btnForgetPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0)
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtPassword.setFocus();
				}
			}
		});
		
		btnForgetPassword.addSelectionListener(new SelectionAdapter() {
		
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
				if(txtUserName.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setText("Error!");
					msg1.setMessage("Please Enter User Name");
					msg1.open();
					
					txtUserName.setFocus();
					
				}
				if(!txtUserName.getText().trim().equals(""))
				{
				if(StartupController.userExists(txtUserName.getText()))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setText("Error!");
					msg1.setMessage("User does not exist");
					msg1.open();
					
					txtUserName.setFocus();
					txtUserName.selectAll();
					return;
					
				
					}
			
				if(txtUserName.getText().equals("admin"))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setText("Error!");
					msg1.setMessage("This Option is not available for Admin");
					msg1.open();
					txtUserName.setFocus();
					txtUserName.selectAll();
					return;
				}
				
			
				else
				{
				Username= txtUserName.getText();
				btnForgetPassword.getShell().getDisplay().dispose();
				ForgetPassword fp = new ForgetPassword(Username);
				}
				}
				
			}
		});
		txtUserName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(txtUserName.getText().trim().equals(""))
				{
					txtUserName.setFocus();
					return;
				}
			
				if(StartupController.userExists(txtUserName.getText()))
				{
					txtUserName.setFocus();
					return;
					
				
					}
			
				
				
					if(txtUserName.getText().trim().equals("admin"))
						
					{

						//btnForgetPassword.setEnabled(false);
						txtPassword.setFocus();
						return;
					
					}
					else
					{

						txtPassword.setFocus();
						//btnForgetPassword.setEnabled(true);
						return;
					}
				

			}
		});
		
		btnBack.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0)
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnLogin.setFocus();
				}
				
			}
		});
		
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				btnBack.getParent().dispose();
				startupForm sf = new startupForm();
			}
		});
					
	}
	
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}
	private void showView()
	{
		try {
			while(! this.isDisposed())
			{
				if(! this.getDisplay().readAndDispatch())
				{
					this.getDisplay().sleep();
					if ( ! this.getMaximized())
					{
						this.setMaximized(true);
					}
				}
				
			}
			this.dispose();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				while(! this.isDisposed())
				{
					if(! this.getDisplay().readAndDispatch())
					{
						this.getDisplay().sleep();
						if ( ! this.getMaximized())
						{
							this.setMaximized(true);
						}
					}
					
				}
				this.dispose();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}


	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LoginForm lf = new LoginForm();
		
		

	}

}