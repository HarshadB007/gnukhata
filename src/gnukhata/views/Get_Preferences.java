package gnukhata.views;
import gnukhata.globals;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
 
public class Get_Preferences extends Dialog {
  String value;
 
  /**
   * @param parent
   */
  public Get_Preferences(Shell parent) {
    super(parent);
  }
 
  /**
   * @param parent
   * @param style
   */
  public Get_Preferences(Shell parent, int style) {
    super(parent, style);
  }
 
  /**
   * Makes the dialog visible.
   *
   * @return
   */
  public String open() {
    Shell parent = getParent();
    final Shell shell =
      new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
    shell.setText("URL Input");
 
    shell.setLayout(new GridLayout(2, true));
 
    Label label = new Label(shell, SWT.NULL);
    label.setText("Please enter &URL address:");
 
    final Text text = new Text(shell, SWT.DOUBLE_BUFFERED| SWT.BORDER);
 
    final Button buttonOK = new Button(shell, SWT.PUSH);
    buttonOK.setText("&Ok");
    buttonOK.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
    final Button buttonCancel = new Button(shell, SWT.PUSH);
    buttonCancel.setText("&Cancel");
 
    text.addListener(SWT.Modify, new Listener() {
      public void handleEvent(Event event) {
        try {
          value = new String("http://"+text.getText()+ ":7081");

          buttonOK.setEnabled(true);
        } catch (Exception e) {
          buttonOK.setEnabled(true);
        }
      }
    });
    
    text.addKeyListener(new KeyListener() {
		
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.keyCode==SWT.CR||arg0.keyCode==SWT.KEYPAD_CR)
			{
				buttonOK.setFocus();
				buttonOK.notifyListeners(SWT.Selection, new Event());
			}
			if(arg0.keyCode==SWT.ESC)
			{
				buttonCancel.notifyListeners(SWT.Selection, new Event());
			}
		}
	});
    
	text.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
			}
						
	if(arg0.keyCode == 46)
	{
		return;
	}
	if(arg0.keyCode == 45||arg0.keyCode == 62)
	{
		  arg0.doit = false;
		
	}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }
        

			
		}
	});
    System.out.println(value); 
    buttonOK.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
    	  try {
    	  if (text.getText().equals(""))
			{
    		  MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
				errMessage.setText("Error!");
				errMessage.setMessage("Please enter the URL");
				errMessage.open();	
				text.setFocus();
				return;
			}
    	  
    	  
    		  try
    		  {
    			  XmlRpcClientConfigImpl	  conf = new XmlRpcClientConfigImpl();
				
				conf.setServerURL(new URL(value));
				globals.client.setConfig(conf);
				Object[] params = new Object[] {};
				Object[] result = (Object[]) gnukhata.globals.client.execute("getOrganisationNames", params);

				System.out.println("result is" + result);
				String[] names = new String[result.length +1];
				names[0] = "--select--";
				// this.finyear[]=new Strng ();
				for (int i = 0; i < result.length; i++)
				{
					System.out.println("result[ " + i + "] is " + result[i]);
					names[i +1] = result[i].toString();
				}
    		  }
    		  catch(Exception e)
    		  {
    			e.printStackTrace();
    			
    			MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
  				errMessage.setText("Error!");
  				errMessage.setMessage("Incorrect URL");
  				errMessage.open();	
  				text.setFocus();
  				return;
    		  }
    		  shell.dispose();
		        
				
			
    	  
    	  }catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
      }
      
    });
    buttonCancel.addListener(SWT.Selection, new Listener() {
      public void handleEvent(Event event) {
    	  XmlRpcClientConfigImpl	  conf = new XmlRpcClientConfigImpl();
			
			try {
				conf.setServerURL(new URL("http://localhost:7081"));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			globals.client.setConfig(conf);
			shell.dispose();
      }
    });
    shell.addListener(SWT.Traverse, new Listener() {
      public void handleEvent(Event event) {
        if(event.detail == SWT.TRAVERSE_ESCAPE)
          event.doit = false;
      }
    });
 
    text.setText("");
    shell.pack();
    shell.open();
 
    Display display = parent.getDisplay();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
 
    return value;
  }
 
  public static void main(String[] args) {
  
  }
}