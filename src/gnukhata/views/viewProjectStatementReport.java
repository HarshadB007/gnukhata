package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.reportmodels.projectstatement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;

public class viewProjectStatementReport extends Composite {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color tableBackground;
	Color tableForeground;
	Color lightBlue;
	int shellwidth;
	int finshellwidth;
	int counter = 0;
	int tblIndex;
	static Display display;
	TableViewer tblprojstmt;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn totaloutgoing;
	TableColumn totalincoming;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lbltotaloutgoing;
	Label lbltotalincoming;
	NumberFormat nf;
	String projname;
	Button btnViewpsForAccount;
	Button btnPrint;
	String strdate;
	
	Vector<Object> printProjectStmt = new Vector<Object>();
	ArrayList<Button> accounts=new ArrayList<Button>();
	static String endDateParam = ""; 
	ODPackage sheetstream;
	
	 public viewProjectStatementReport(Composite parent,String toDate, int style, ArrayList<projectstatement> prjstmt,String selectproject)
	{
		
		super(parent,style);
		// TODO Auto-generated constructor stub
		
		projname=selectproject;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		//Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		//layout.top = new FormAttachment(1);
		//layout.left = new FormAttachment(63);
		//layout.right = new FormAttachment(87);
		//layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);

		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText("For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		lblLine.setLayoutData(layout);

		Label lblheadline=new Label(this, SWT.NONE);
		strdate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0, 4);
		endDateParam = toDate;
		lblheadline.setText("Statement for Project:"+projname.toString().replace("&", "&&")+"\t\t\t\t\t\t\t\t\t\t\t\t\tPeriod "+"From  "+globals.session[2]+" To  "+strdate);
		lblheadline.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(7);
		layout.bottom = new FormAttachment(8);
		lblheadline.setLayoutData(layout);
		
				
		tblprojstmt = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblprojstmt.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		tblprojstmt.getTable().setLinesVisible (true);
		tblprojstmt.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(92);
		tblprojstmt.getTable().setLayoutData(layout);
	
		btnViewpsForAccount =new Button(this,SWT.PUSH);
		btnViewpsForAccount.setText("&Back");
		btnViewpsForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblprojstmt.getTable(),10);
		layout.left=new FormAttachment(25);
		btnViewpsForAccount.setLayoutData(layout);

		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblprojstmt.getTable(),10);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);
		
		this.makeaccessible(tblprojstmt.getTable());
		this.getAccessible();
		
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		shellwidth = this.getClientArea().width;
		finshellwidth = shellwidth-(2*shellwidth/100);
		
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		tableBackground = new Color(this.getDisplay(),255, 255, 214);
		tableForeground =  new Color(this.getDisplay(),184, 255, 148);
		lightBlue = new Color(this.getDisplay(),215,242,251);
		globals.setThemeColor(this, Background, Foreground);
		tblprojstmt.getControl().setBackground(lightBlue);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		//globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		this.setReport(prjstmt);
		this.setEvents(prjstmt);
		
		try {
			sheetstream=ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/ProjectStatement.ots"), "ProjectStatement");
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	 
	private void setReport(ArrayList<projectstatement> prjstmt)
	{
		final TableViewerColumn colSrNo = new TableViewerColumn(tblprojstmt, SWT.None);
		colSrNo.getColumn().setText("Sr.No.");
		colSrNo.getColumn().setAlignment(SWT.LEFT);
		colSrNo.getColumn().setWidth(4 * finshellwidth /100);
		
		colSrNo.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				
				gnukhata.controllers.reportmodels.projectstatement prj= (gnukhata.controllers.reportmodels.projectstatement)element;
					
				return prj.getSrNo();
				//return super.getText(element);
			}
		}
		);
		
		final TableViewerColumn colaccname = new TableViewerColumn(tblprojstmt, SWT.None);
		colaccname.getColumn().setText("                                     Account Name");
		colaccname.getColumn().setAlignment(SWT.LEFT);
		colaccname.getColumn().setWidth(30 * finshellwidth /100);
		
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.projectstatement accname = (projectstatement) element;
				return accname.getAccountName();
				//return super.getText(element);
			}
		}
		);
		
		final TableViewerColumn colgrpname = new TableViewerColumn(tblprojstmt, SWT.None);
		colgrpname.getColumn().setText("                 Group Name");
		colgrpname.getColumn().setAlignment(SWT.LEFT);
		colgrpname.getColumn().setWidth(22 * finshellwidth /100);
		
		colgrpname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.projectstatement grpname = (projectstatement) element;
				return grpname.getGroupName();
				//return super.getText(element);
			}
		}
		);
		
		
		final TableViewerColumn coltotalout = new TableViewerColumn(tblprojstmt, SWT.None);
		coltotalout.getColumn().setText("Total Outgoing                      ");
		coltotalout.getColumn().setAlignment(SWT.RIGHT);
		
		if (globals.session[8].toString().equals("gnome")) 
		{
			coltotalout.getColumn().setWidth(22 * finshellwidth / 100);
		}
		if (globals.session[8].toString().equals("ubuntu")) 
		{
			coltotalout.getColumn().setWidth(19 * finshellwidth / 100);
		}
		coltotalout.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.projectstatement ttloutgoing = (projectstatement) element;
				try {
					
					Double totalout = Double.parseDouble(ttloutgoing.getTotalOutgoing());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(totalout);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}

				//return super.getText(element);
			}
		}
		);
		
		final TableViewerColumn coltotalin = new TableViewerColumn(tblprojstmt, SWT.None);
		coltotalin.getColumn().setText("Total Incoming                       ");
		coltotalin.getColumn().setAlignment(SWT.RIGHT);
		if (globals.session[8].toString().equals("gnome")) 
		{
			coltotalin.getColumn().setWidth(16 * finshellwidth / 100);
		}
		if (globals.session[8].toString().equals("ubuntu"))
		{
			coltotalin.getColumn().setWidth(16 * finshellwidth / 100);
		}
		coltotalin.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.projectstatement ttlincoming = (gnukhata.controllers.reportmodels.projectstatement) element;
				try {
					
					Double totalin = Double.parseDouble(ttlincoming.getTotalIncoming());
					System.out.println("total");
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(totalin);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
					return "";
				}

				//return super.getText(element);
			}
		}
		);
		
		
		
		tblprojstmt.setContentProvider(new ArrayContentProvider());
		tblprojstmt.setInput(prjstmt);
		TableItem[] items = tblprojstmt.getTable().getItems();
		for (int rowid=0; rowid<items.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items[rowid].setBackground(tableBackground);
		    }
		    else {
		    	items[rowid].setBackground(tableForeground);
		    }
		}
		tblprojstmt.getTable().setFocus();
		tblprojstmt.getTable().setSelection(0);

	}
	
	
	private void setEvents(final ArrayList<projectstatement> prjstmt)
	{
		btnViewpsForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
			}
		});
		
		tblprojstmt.getControl().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblIndex=tblprojstmt.getTable().getSelectionIndex();
				tblprojstmt.getTable().setSelection(-1);
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblprojstmt.getTable().setSelection(tblIndex);
			}
		});
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewpsForAccount.setFocus();
				}
			}
		});
		
		btnViewpsForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewpsForAccount.getParent().getParent();
				btnViewpsForAccount.getParent().dispose();
					
					ViewProjectStatement vl=new ViewProjectStatement(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
		
		
		//tblprojstmt.setFocus();
		
		tblprojstmt.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) tblprojstmt.getSelection();
				projectstatement pjsmt = (projectstatement) selection.getFirstElement();
				if(pjsmt.getGroupName().equals("Total"))
				{
					return;
				}
				String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
				Composite grandParent = (Composite) tblprojstmt.getTable().getParent().getParent();
				String accName = pjsmt.getAccountName();
				reportController.showLedger(grandParent, accName, fromdate, endDateParam, projname, true, false, true, "", projname);
					tblprojstmt.getTable().getParent().dispose();
					
					
				}
			});
		
		tblprojstmt.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
				{
					//drilldown here, make a call to showLedger.
					IStructuredSelection selection = (IStructuredSelection) tblprojstmt.getSelection();
					projectstatement pjsmt = (projectstatement) selection.getFirstElement();
					try {
						if(pjsmt.getGroupName().equals("Total"))
						{
							return;
						}
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
						Composite grandParent = (Composite) tblprojstmt.getTable().getParent().getParent();
						String accName = pjsmt.getAccountName();
						reportController.showLedger(grandParent, accName, fromdate, endDateParam, projname, true, false, true, "", projname);
						tblprojstmt.getTable().getParent().dispose();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					



					
				}
				//super.keyPressed(arg0);
			}
		});

		
		
			btnPrint.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					btnViewpsForAccount.setFocus();
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					String[] strPrintCol = new String[]{"","","","",""};
					Object[][] finaldata=new Object[printProjectStmt.size() +1][strPrintCol.length];
					Object[] firstRow = new Object[]{"Sr.No","Account Name","Group Name","Total Outgoing","Total Incoming" };
					finaldata[0] = firstRow;

					
					
					for(int counter=0; counter < printProjectStmt.size(); counter++)
					{
						Object[] printrow=(Object[])printProjectStmt.get(counter);
				//		orgdata[counter]=printrow;
						finaldata[counter +1]=printrow;
					}	
					//printLedgerData.copyInto(finalData);
					
					
					TableModel model = new DefaultTableModel(finaldata,strPrintCol);
					try 
					{
						final File ProjectStatement = new File("/tmp/gnukhata/Report_Output/ProjectStatement" );
						//final File ProjectstmtTemplate =  new File("Report_Templates/ProjectStatement.ots");
						final Sheet ProjectstmtReportSheet = sheetstream.getSpreadSheet().getFirstSheet();
						ProjectstmtReportSheet.ensureRowCount(100000);
						ProjectstmtReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
						ProjectstmtReportSheet.getCellAt(0, 1).setValue("Statement for Project:"+projname+"\t\t\t\t\t\t\t\t\tPeriod "+"From  "+globals.session[2]+" To  "+strdate);
						for(int rowcounter = 0; rowcounter < prjstmt.size(); rowcounter ++ )
						{
							ProjectstmtReportSheet.getCellAt(0,rowcounter +3).setValue(prjstmt.get(rowcounter).getSrNo());
							ProjectstmtReportSheet.getCellAt(1,rowcounter +3).setValue(prjstmt.get(rowcounter).getAccountName());
							ProjectstmtReportSheet.getCellAt(2,rowcounter +3).setValue(prjstmt.get(rowcounter).getGroupName());
							ProjectstmtReportSheet.getCellAt(3,rowcounter +3).setValue(prjstmt.get(rowcounter).getTotalOutgoing());
							ProjectstmtReportSheet.getCellAt(4,rowcounter +3).setValue(prjstmt.get(rowcounter).getTotalIncoming());
						}
						OOUtils.open(ProjectstmtReportSheet.getSpreadSheet().saveAs(ProjectStatement));
						//OOUtils.open(AccountReport);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});				
		
		}
		
	

	 public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
}