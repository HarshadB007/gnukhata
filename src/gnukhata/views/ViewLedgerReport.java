package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;
import gnukhata.controllers.reportmodels.transaction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
/*import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
*/

public class ViewLedgerReport extends Composite {

	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color lightBlue; 
	Color tabalternate; 
    Color tabalternate1;
	
    int tblfocusindex;
    
	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	Color bgbtnColor;
	Color fgbtnColor;
	Color bgcomboColor;
	Color fgcomboColor;
	Color bgtxtColor;
	Color fgtxtColor;
	Color bgtblColor;
	Color fgtblColor;
	int counter=0;
	ODPackage sheetStream, sheetStreamNar;
	
	TableViewer ledgerReport;
	TableColumn Date;
	TableColumn Particulars;
	TableColumn VoucherNumber;
	TableColumn Dr;
	TableColumn Cr;
	TableColumn Narration;
	
	//TableItem headerRow;
	Label lblOrgDetails;
	Label lblheadline;
	Label lblorgname;
	Label lblDate;
	Label lblParticulars;
	Label lblVoucherNumber;
	Label lblNarration;
	Label lblDr;
	Label lblCr;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblPageName;
	Label lblPeriod;
	TableEditor DateEditor;
	TableEditor ParticularEditor;
	TableEditor VoucherNumberEditor;
	TableEditor DrEditotr;
	TableEditor CrEditor;
	TableEditor NarrationEditor;
	static Display display;
	
	Button btnViewLedgerForPeriod;
	Button btnViewLedgerForAccount;
	Button btnPrint;
	Button btnViewDualLedger;
	
	String tb;
	String strFromDate;
	String strToDate;
	ArrayList<Button> voucherCodes = new ArrayList<Button>();
	
	String accountName="";
	String startDate="";
	String endDate="";
	String projectName;
	String ledgerProject;
	boolean narration1;
	boolean tbflag;
	boolean projectflag;
	boolean ledgerflag;
	boolean dualflag;
	String oldaccname;
	String oldfromdate;
	String oldenddate;
	String oldselectproject;
	String oldprojectname;
	String searchValues[] = null;
	boolean oldnarration;
	Vector<Object> printLedgerData = new Vector<Object>();
	int shellwidth = 0;
	int finshellwidth;
	
	public ViewLedgerReport(Composite parent, int style, ArrayList<transaction> lstLedger,String ProjectName,String oldprojectname1, boolean narrationFlag, boolean narration, String accountName,String oldaccname1,String frmDate,String oldfromdate1,String toDate,String oldenddate1, boolean tbdrilldown, boolean psdrilldown,String tbType,String selectproject,String oldselectproject1,boolean dualledgerflag) {
		super(parent, style);
		// TODO Auto-generated constructor stub
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		//txtaccname.setText(result[0].toString());
		
		/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+selectproject);
		msg.open();*/
		
		accname=accountName;
		narration1= narrationFlag;
		tbflag = tbdrilldown;
		projectflag =psdrilldown;
		startDate=frmDate;
		endDate=toDate;
		tb = tbType;
		projectName=selectproject;
		ledgerProject=ProjectName;
		dualflag=dualledgerflag;
		
		//old values
		this.oldaccname=oldaccname1;
		this.oldfromdate=oldfromdate1;
		this.oldenddate=oldenddate1;
		this.oldselectproject=oldselectproject1;
		this.oldprojectname=oldprojectname1;
		this.oldnarration=narration;
		
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    strFromDate=frmDate.substring(8)+"-"+frmDate.substring(5,7)+"-"+frmDate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
		//Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		/*lblLogo.setLocation(getClientArea().width, getClientArea().height);*/
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);

		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText("For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		
		lblLine.setLayoutData(layout);
		
		
	

		/* lblorgname=new Label(this, SWT.NONE);
		lblorgname.setFont( new Font(display,"Times New Roman", 14, SWT.NORMAL | SWT.BOLD) );
	
		lblorgname.setText(globals.session[1].toString());
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(5);
		lblorgname.setLayoutData(layout);*/
		

		Label lblAccName=new Label(this, SWT.NONE);
		lblAccName.setFont( new Font(display,"Times New Roman", 12, SWT.NORMAL | SWT.BOLD) );
		/*if(! ProjectName.equals("No Project"))
		{*/
		lblAccName.setText("Account Name: "+accname.replaceAll("&","&&"));
		/*}*/
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,2);
		layout.left = new FormAttachment(5);
		layout.bottom = new FormAttachment(10);
		lblAccName.setLayoutData(layout);
		

		lblPageName = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		lblPageName.setFont(new Font(display, "Times New Roman", 12, SWT.NORMAL | SWT.BOLD));
		lblPageName.setText("Period From "+strFromDate+" To "+strToDate);
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(40);
		layout.bottom = new FormAttachment(8);
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblPageName.setLayoutData(layout);
		
		Label lblPrjName=new Label(this, SWT.NONE);
		lblPrjName.setFont( new Font(display,"Times New Roman", 12, SWT.NORMAL | SWT.BOLD) );
		//lblPrjName.setText("Project: "+ProjectName);
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(85);
		layout.bottom = new FormAttachment(8);
		lblPrjName.setLayoutData(layout);
		if(ProjectName=="No Project" || ProjectName=="")
		{
			lblPrjName.setVisible(false);
		}
		else
		{
			lblPrjName.setText("Project: "+ProjectName.toString().replace("&", "&&"));
		}
		
		/*MessageBox msg1=new MessageBox(new Shell(),SWT.OK);
		msg.setMessage("Project name"+selectproject);
		msg.open();*/
		
		/*lblPeriod = new Label(this, SWT.NONE);
		lblPeriod.setText("Period: "+strFromDate+"  To "+strToDate);
		lblPeriod.setFont(new Font(display, "Times New Roman", 14, SWT.NORMAL | SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(55);
		//layout.right = new FormAttachment(83);
		//layout.bottom = new FormAttachment(31);
		lblPeriod.setLayoutData(layout);*/
		
		ledgerReport = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    ledgerReport.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
	    ledgerReport.getTable().setLinesVisible (true);
		ledgerReport.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblAccName,5);
		
		layout.left = new FormAttachment(0);
		layout.right = new FormAttachment(100);
		
		layout.bottom = new FormAttachment(92);
		ledgerReport.getTable().setLayoutData(layout);
		
		ledgerReport.getControl().forceFocus();
		
		//ledgerReport.getControl().setBackground(Display.getDefault().getSystemColor(SWT.COLOR_CYAN));
		//ledgerReport.getControl().setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
		
		btnViewDualLedger=new Button(this,SWT.PUSH);
		if(psdrilldown == false)
		{
			btnViewDualLedger.setText("&Dual Ledger Account");
			 btnViewDualLedger.setData("psdrilldown",psdrilldown );
		if(tbdrilldown == false)
		{
			btnViewDualLedger.setText("&Dual Ledger Account");
			btnViewDualLedger.setData("tbdrilldown",tbdrilldown );
		}
		else
		{
			btnViewDualLedger.setData("tbdrilldown",tbdrilldown );
			btnViewDualLedger.setData("enddate",toDate  );
			btnViewDualLedger.setData("tbtype", tbType  );
			btnViewDualLedger.setText("&Back");
		}
		
		
		}
		else
		{
			btnViewDualLedger.setData("psdrilldown",psdrilldown );
			btnViewDualLedger.setData("enddate",toDate  );
			btnViewDualLedger.setData("selectproject", selectproject  );
			btnViewDualLedger.setText("&Back");
		}
		
		
		btnViewDualLedger.setFont(new Font(display, "Times New Roman", 10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReport.getTable(),8);
		//layout.left=new FormAttachment(30);
		layout.left = new FormAttachment(15);
		btnViewDualLedger.setLayoutData(layout);
		//btnViewDualLedger.setFocus();
		
		
		
		btnViewLedgerForAccount=new Button(this,SWT.PUSH);
		btnViewLedgerForAccount.setFont( new Font(display,"Times New Roman", 10, SWT.NORMAL | SWT.BOLD) );
		if(psdrilldown == false||tbdrilldown==false)
		{
				btnViewLedgerForAccount.setText("&View Another Ledger");
		
				 btnViewLedgerForAccount.setData("psdrilldown",psdrilldown );
				// btnViewLedgerForAccount.setVisible(false);
				 btnViewLedgerForAccount.setData("tbdrilldown",tbdrilldown );
					
		}

		if(tbdrilldown==true)
		{
			btnViewLedgerForAccount.setData("tbdrilldown",tbdrilldown );
			btnViewLedgerForAccount.setData("enddate",toDate  );
			btnViewLedgerForAccount.setData("tbtype", tbType  );
			btnViewLedgerForAccount.setText("&Back");
			layout = new FormData();
			layout.top = new FormAttachment(ledgerReport.getTable(),8);
			layout.left = new FormAttachment(35);
			//layout.right = new FormAttachment(45);
			btnViewLedgerForAccount.setVisible(false);
		}
		
		
		
		if(psdrilldown==true)
		{
			btnViewLedgerForAccount.setData("psdrilldown",psdrilldown );
			btnViewLedgerForAccount.setData("enddate",toDate  );
			btnViewLedgerForAccount.setData("selectproject", selectproject  );
			btnViewLedgerForAccount.setText("&Back");
		}
		
		btnViewLedgerForAccount.setFont(new Font(display, "Times New Roman", 10,SWT.BOLD));
		layout = new FormData();
		btnViewLedgerForAccount.setText("&View Another Ledger");
		
		layout.top=new FormAttachment(ledgerReport.getTable(),8);
		layout.left = new FormAttachment(30);
		btnViewLedgerForAccount.setLayoutData(layout);
		
	
	
		
		btnViewLedgerForPeriod=new Button(this, SWT.PUSH);
		btnViewLedgerForPeriod.setText(" View &Ledger For Another Period ");
		btnViewLedgerForPeriod.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReport.getTable(),8);
		layout.left=new FormAttachment(45);
		btnViewLedgerForPeriod.setLayoutData(layout);
		if(psdrilldown==true)
		{
			btnViewLedgerForPeriod.setVisible(false);
		}
		if(tbdrilldown==true)
		{
			btnViewLedgerForPeriod.setVisible(false);
		}
		
		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(ledgerReport.getTable(),8);
		layout.left=new FormAttachment(65);
		btnPrint.setLayoutData(layout);
		
				
	  
	    this.getAccessible();
	    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	    
	    
		shellwidth = this.getClientArea().width;
		finshellwidth = shellwidth-(shellwidth*2/100);
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/Ledger.ots"),"Ledger");
			sheetStreamNar = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/LedgerNarration.ots"),"LedgerNarration");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		tabalternate =  new Color(this.getDisplay(),255, 255, 214);
		tabalternate1 =  new Color(this.getDisplay(),184, 255, 148);
		
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		lightBlue = new Color(this.getDisplay(),215,242,251);
		globals.setThemeColor(this, Background, Foreground);
		ledgerReport.getControl().setBackground(lightBlue);
        globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        //globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		
		this.setReport(lstLedger, narrationFlag);
	    this.setEvents(lstLedger);
		   

	}
	private void setReport(ArrayList<transaction> lstLedger,boolean narrationFlag)
	{
		TableViewerColumn coltransactiondate = new TableViewerColumn(ledgerReport, SWT.None);
		coltransactiondate.getColumn().setText("Date");
		coltransactiondate.getColumn().setAlignment(SWT.CENTER);
		if(globals.session[8].toString().equals("gnome"))
		{
			coltransactiondate.getColumn().setWidth(9 * finshellwidth /100);
		}
		if(globals.session[8].toString().equals("ubuntu"))
		{
			coltransactiondate.getColumn().setWidth(7 * finshellwidth /100);
		}
		coltransactiondate.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.transaction transactiondate = (gnukhata.controllers.reportmodels.transaction) element;
			return transactiondate.getTransactionDate();
			}
		});
		TableViewerColumn colvno  = new TableViewerColumn(ledgerReport, SWT.None);
		colvno.getColumn().setText("V.No");
		colvno.getColumn().setAlignment(SWT.CENTER);
		colvno.getColumn().setWidth(5 * finshellwidth /100);
		colvno.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.transaction vno = (gnukhata.controllers.reportmodels.transaction) element;
			return vno.getVoucherNo();
			}
		});

		TableViewerColumn colLock  = new TableViewerColumn(ledgerReport, SWT.None);
		colLock.getColumn().setText("Status");
		colLock.getColumn().setAlignment(SWT.CENTER);
		colLock.getColumn().setWidth(4 * finshellwidth /100);
		colLock.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.transaction vno = (gnukhata.controllers.reportmodels.transaction) element;
			int lockflag;
			try {
				lockflag = transactionController.getLockFlag(vno.getVoucherCode());
				if (lockflag==1) {
					return "*";
				}
				else{
					return "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
			
			}
		});
		
		
		TableViewerColumn colparticulars = new TableViewerColumn(ledgerReport, SWT.None);
		colparticulars.getColumn().setText("                  Particulars");
		colparticulars.getColumn().setAlignment(SWT.LEFT);
		if(globals.session[8].toString().equals("gnome"))
		{
			colparticulars.getColumn().setWidth(66 * finshellwidth /100);
		}
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colparticulars.getColumn().setWidth(65 * finshellwidth /100);
		}
		
		colparticulars.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.transaction particulars = (gnukhata.controllers.reportmodels.transaction) element;
			return particulars.getParticulars();
			}
		});	
		
			
		
		TableViewerColumn  coldr = new TableViewerColumn(ledgerReport, SWT.None);
		coldr.getColumn().setText("Debit                ");
		coldr.getColumn().setAlignment(SWT.RIGHT);
		
		coldr.getColumn().setWidth(8 * finshellwidth /100);
		
		coldr.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.transaction dr  = (gnukhata.controllers.reportmodels.transaction) element;
			return dr.getDr();
			}
		});
		
		TableViewerColumn colcr = new TableViewerColumn(ledgerReport, SWT.None);
		colcr.getColumn().setResizable(false);
		colcr.getColumn().setText("Credit                ");
		colcr.getColumn().setAlignment(SWT.RIGHT);
		
			colcr.getColumn().setWidth(8* finshellwidth /100);
		
		
		colcr.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.transaction cr  = (gnukhata.controllers.reportmodels.transaction) element;
			return cr.getCr();
			}
		});	
		
		
		
		 ledgerReport.setContentProvider(new ArrayContentProvider());
		 ledgerReport.setInput(lstLedger);
		 
		 TableItem[] items1 = ledgerReport.getTable().getItems();
			for (int rowid=0; rowid<items1.length; rowid++){
			    if (rowid%2==0) 
			    {
			    	items1[rowid].setBackground(tabalternate);
			    }
			    else {
			    	items1[rowid].setBackground(tabalternate1);
			    }
			}
		 
		 ledgerReport.getTable().setSelection(0);
		 
		 //ledgerReport.getTable().pack();
		/* MessageBox msgdisplay = new MessageBox(new Shell(),SWT.OK);
		msgdisplay.setMessage(Integer.toString(this.getClientArea().width ));
		msgdisplay.open();*/
		 ledgerReport.getTable().setFocus();				/**/		//ledgerReport.pack();


	}
	
	
	public void setEvents(final ArrayList<transaction> lstLedger)
	{
		ledgerReport.getTable().addFocusListener(new FocusAdapter() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				tblfocusindex = ledgerReport.getTable().getSelectionIndex();
				ledgerReport.getTable().setSelection(-1);
			}
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				ledgerReport.getTable().setSelection(tblfocusindex);
			}
		});
		


		
		
		btnViewLedgerForPeriod.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Composite grandParent=(Composite)btnViewLedgerForPeriod.getParent().getParent();
				btnViewLedgerForPeriod.getParent().dispose();
				ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,"","","","",false,false,false,"","",false);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				vl.dropdownAccountName.select(vl.dropdownAccountName.indexOf(accname));
				vl.dropdownAccountName.setEnabled(false);
				vl.txtFromDtDay.setFocus();
				vl.btnView.setEnabled(true);
			}
		});
		
		btnViewLedgerForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewLedgerForAccount.getParent().getParent();
				
				if( (Boolean) btnViewLedgerForAccount.getData("psdrilldown") == false )
				{
				if( (Boolean) btnViewLedgerForAccount.getData("tbdrilldown") == false )
				{
					btnViewLedgerForAccount.getParent().dispose();
					
					ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,"","","","",false,false,false,"","",false);
						vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
				else
				{
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String tbtype = btnViewLedgerForAccount.getData("tbtype").toString();
					btnViewLedgerForAccount.getParent().dispose();
					reportController.showTrialBalance(grandParent, enddate, tbtype);
				}
				}

				else
				{
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String sp = btnViewLedgerForAccount.getData("selectproject").toString();
					btnViewLedgerForAccount.getParent().dispose();
					reportController.showProjectStatement(grandParent, enddate, sp);
				}
		
		
		
			}
		
	});
		
		
		btnViewDualLedger.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//String accnamesent=btnViewDualLedger.getData("accname").toString();
				
				
				
				Composite grandParent = (Composite) btnViewDualLedger.getParent().getParent();
				
				
				if( (Boolean) btnViewDualLedger.getData("psdrilldown") == false )
				{
				if( (Boolean) btnViewDualLedger.getData("tbdrilldown") == false )
				{
				/*	String accnamesent=btnViewDualLedger.getData("accname").toString();
					String fromDate = btnViewDualLedger.getData("startDate").toString();
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String projectName=btnViewDualLedger.getData("ledgerProject").toString();
					String narrationflag1=btnViewDualLedger.getData("narration").toString();
					String tbDrilldown=btnViewDualLedger.getData("tbflag").toString();
					String psDrilldown=btnViewDualLedger.getData("projectflag").toString();
					String tbtype = btnViewLedgerForAccount.getData("tb").toString();	
					String selectproject=btnViewDualLedger.getData("projectName").toString();
				*/	btnViewDualLedger.getParent().dispose();
				if(narration1==false)
				{
			
				ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,accname,startDate,endDate,ledgerProject,false,false,false,"","",true);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
					if(narration1==true)
					{
						ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,accname,startDate,endDate,ledgerProject,true,false,false,"","",true);
						vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
			
					}
				}
				else
				{
					/*String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String tbtype = btnViewLedgerForAccount.getData("tbtype").toString();
			*/	
					btnViewDualLedger.getParent().dispose();
					reportController.showTrialBalance(grandParent, endDate, tb);
				}
				}

				else
				{
					/*String enddate = btnViewDualLedger.getData("enddate").toString();
					String sp = btnViewDualLedger.getData("selectproject").toString();
					*/
					btnViewDualLedger.getParent().dispose();
					reportController.showProjectStatement(grandParent, endDate, projectName);
				}
		}
		
		
		
		
	});

		
		
		
		btnViewLedgerForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnViewLedgerForPeriod.setFocus();
				}
			}
		});
		
		btnViewLedgerForPeriod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
			}
		});
		
	
		
		ledgerReport.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) ledgerReport.getSelection();
				transaction trn = (transaction) selection.getFirstElement();
				try {
					if(trn.getParticulars().trim().equals("") ||trn.getParticulars().trim().equals("Total of Transactions") || trn.getParticulars().trim().equals("Grand Total")|| trn.getParticulars().trim().equals("Closing Balance c/f")|| trn.getParticulars().trim().equals("Opening Balance b/f"))
						{
							return;
						}
						int vouchercode= trn.getVoucherCode();
						int lockflag = transactionController.getLockFlag(vouchercode);
						Composite grandParent = (Composite) ledgerReport.getTable().getParent().getParent();
						try {
							transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb, true, startDate,oldfromdate, endDate,oldenddate, accname,oldaccname, ledgerProject,oldprojectname, narration1,oldnarration, projectName,oldprojectname,dualflag,lockflag,0,searchValues);
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ledgerReport.getTable().getParent().dispose();
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				//super.mouseDoubleClick(arg0);
			}
		});

		ledgerReport.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
				{
					//drilldown here, make a call to showLedger.
					IStructuredSelection selection = (IStructuredSelection) ledgerReport.getSelection();
					transaction trn1 = (transaction) selection.getFirstElement();
					try {
						if(trn1.getParticulars().trim().equals("") ||trn1.getParticulars().trim().equals("Total of Transactions") || trn1.getParticulars().trim().equals("Grand Total")|| trn1.getParticulars().trim().equals("Closing Balance c/f")|| trn1.getParticulars().trim().equals("Opening Balance b/f"))
							{
								return;
							}
							int vouchercode= trn1.getVoucherCode();
							int lockflag = transactionController.getLockFlag(vouchercode);
							Composite grandParent = (Composite) ledgerReport.getTable().getParent().getParent();
							try {
								
								transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb, true, startDate,oldfromdate, endDate,oldenddate, accname,oldaccname, ledgerProject,oldprojectname, narration1,oldnarration, projectName,oldprojectname,dualflag,lockflag,0,searchValues);
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							ledgerReport.getTable().getParent().dispose();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
			}

			});
		

		/*ledgerReport.getControl().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				super.focusGained(arg0);
				
				ledgerReport.getControl().setBackground(Display.getDefault().getSystemColor(SWT.COLOR_CYAN));
				ledgerReport.getControl().setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
			}@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				super.focusLost(arg0);
				
				ledgerReport.getControl().setBackground(bgtblColor);
				ledgerReport.getControl().setForeground(fgtblColor);
			}
		});*/
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewDualLedger.setFocus();
				}
			}
		});
		
		btnViewDualLedger.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnViewLedgerForAccount.setFocus();
				}
			}
		});
		
/*		for(int voucherCodeCounter=0; voucherCodeCounter < voucherCodes.size(); voucherCodeCounter++)
		{
			voucherCodes.get(voucherCodeCounter).addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					Button btncurrent=(Button) arg0.widget;
					int vouchercode= Integer.valueOf(arg0.widget.getData("vouchercode").toString());
					
					Composite grandParent=(Composite) btncurrent.getParent().getParent().getParent();
					btncurrent.getParent().getParent().dispose();
					//transactionController.showVoucherDetail(grandParent,"", vouchercode,true);
					
					//transactionController.showVoucherDetail(grandParent, "", vouchercode, tbflag, projectflag, tb, true, startDate, endDate, accname, narration, projectName);
					//transactionController.showVoucherDetail(grandParent, "", vouchercode, tbflag, projectflag, tb1, true, startDate,oldfromdate1, endDate,oldenddate1, accountName1,oldaccName1, ledgerProject,oldprojectName1, narrationflag,narration1, projectName,oldselectproject1,dualflag1);

					transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb, true, startDate,oldfromdate, endDate,oldenddate, accname,oldaccname, ledgerProject,oldprojectname, narration1,oldnarration, projectName,oldprojectname,dualflag);
				}
			});
			
			voucherCodes.get(voucherCodeCounter).addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < voucherCodes.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < voucherCodes.size())
						{
							
							voucherCodes.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0&& counter < voucherCodes.size())
						{
							
							voucherCodes.get(counter).setFocus();
						}
					}
				
					
				}
			});
		}
		
		ledgerReport.setFocus();*/
		/*ledgerReport.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(voucherCodes.isEmpty())
				{
					btnViewDualLedger.setFocus();
				
				}
				else
				{
					voucherCodes.get(0).setFocus();
				}
			}
		});*/
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
									
									try {
										final File ledger = new File("/tmp/gnukhata/Report_Output/ledger" );
		//										SpreadSheet.createEmpty(model).saveAs(ledger);
										
										
											final Sheet ledgerSheet = sheetStream.getSpreadSheet().getFirstSheet();
//											ledgerSheet.getColumn(1).setWidth(new Integer(50));
											ledgerSheet.ensureRowCount(100000);
											ledgerSheet.getCellAt(0,0).setValue(globals.session[1].toString());
											ledgerSheet.getCellAt(0,1).setValue("Ledger Account");
											if(ledgerProject=="No Project" || ledgerProject=="")
											{
												ledgerSheet.getCellAt(0,2).setValue("Account Name: "+ accname+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Period From "+strFromDate+" To "+strToDate);
											}
											else
											{
												ledgerSheet.getCellAt(0,2).setValue("Account Name: "+ accname+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Period From "+strFromDate+" To "+strToDate+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" "+" Project Name: "+ledgerProject);	
											}
										
										
											for(int rowcounter=0; rowcounter<lstLedger.size(); rowcounter++)
											{
												
												ledgerSheet.getCellAt(0,rowcounter+4).setValue(lstLedger.get(rowcounter).getTransactionDate());
												ledgerSheet.getCellAt(1,rowcounter+4).setValue(lstLedger.get(rowcounter).getVoucherNo());
												ledgerSheet.getCellAt(2,rowcounter+4).setValue(lstLedger.get(rowcounter).getParticulars());
												ledgerSheet.getCellAt(3,rowcounter+4).setValue(lstLedger.get(rowcounter).getDr());
												ledgerSheet.getCellAt(4,rowcounter+4).setValue(lstLedger.get(rowcounter).getCr());
												
												//System.out.println("This is prj name:"+ledgerProject );
											}
										
										OOUtils.open(ledgerSheet.getSpreadSheet().saveAs(ledger));
										

//						OOUtils.open(ledger);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
				
				


			}
		});
		
		
}
}
	

	
