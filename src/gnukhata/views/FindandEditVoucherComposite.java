package gnukhata.views;


import gnukhata.globals;
import gnukhata.controllers.transactionController;
import gnukhata.controllers.reportmodels.VoucherDetail;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;


public class FindandEditVoucherComposite extends Composite 
{	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color lightBlue;
	Color tabalternate; 
    Color tabalternate1;
	
    int lock;
	int tblfocusindex;
	
	static Display display;
	public static String typeFlag;
	int counter=0;
	Label lblVoucherNo;
	Label lblNarration;
	Text txtnarration;
	Button save;
	Label lblsearchRec;
	CCombo combosearchRec;
	Label lblEntVoucherNo;
	Text txtEntVoucherNo;
	Text txtFromddate;
	Label lblFromDtDash1;
	Label lblNarrationHeader;
	Label lblFromDate;
	Text txtFromMdate;
	Label lblFromDtDash2;
	Text txtFromYrdate;
	Label lblToDate;
	Text txtToDdate;
	Label lblToDateDash1;
	Text txtToMdate;
	Label lblToDateDash2;
	Text txtToYrdate;
	Label lblentamount;
	Text txtentamount;
	//Button btndelete;
	Button btnsearch;
	Table tableVoucherRecord;
	int searchFlag = 0;
	int totalWidth = 0;
	String searchValues[];
	
	boolean verifyFlag=false;
	
	Object[] projectlist;
	NumberFormat nf;
	boolean findvoucher;
  
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	
	TableViewer tblvoucherView;
	TableViewerColumn colvNo;
	TableViewerColumn  colvType;
	TableViewerColumn coldateOfTran;
	TableViewerColumn coldrAcc;
	TableViewerColumn colcrAcc;
	TableViewerColumn colamount;
	TableViewerColumn colnarr;
	TableViewerColumn colprojName;
	int shellwidth = 0;
	int finshellwidth;
	   
	
	public FindandEditVoucherComposite(Composite parent, int style, boolean findvoucherflag, boolean backFlag, int search_flag, String[] search_values) {
		
		super(parent, style);
		
		findvoucher=findvoucherflag;
		
		Date today = new Date();
		String strToday = sdf.format(today);
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		FormData  layout1 = new FormData();   
		 Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+" For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout1 = new FormData();
		layout1.top = new FormAttachment(1);
		layout1.left = new FormAttachment(2);
		/*layout.right = new FormAttachment(62);
		layout.bottom = new FormAttachment(7);*/
		lblOrgDetails.setLayoutData(layout1);
		
		Label lblLogo = new Label(this, SWT.None);
		layout1 = new FormData();
		layout1.top = new FormAttachment(0);
		layout1.left = new FormAttachment(74);
		/*layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(10);*/
		lblLogo.setLayoutData(layout1);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
			

		Label lblLine = new Label(this, SWT.NONE);
		lblLine.setText("------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout1 = new FormData();
		layout1.top = new FormAttachment(7);
		layout1.left = new FormAttachment(2);
		layout1.right = new FormAttachment(99);
		lblLine.setLayoutData(layout1);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		// Find/Edit/delete Records
		lblsearchRec = new Label(this,SWT.NONE);
		lblsearchRec.setText("S&earch Record By : ");
		lblsearchRec.setFont(new Font(display, "Time New Roman",10,SWT.NORMAL));
		FormData layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(12);
		//layout.bottom = new FormAttachment(11);
		lblsearchRec.setLayoutData(layout);
		
		combosearchRec = new CCombo(this,SWT.READ_ONLY | SWT.BORDER);
		combosearchRec.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
		combosearchRec.add("           ---- Please Select -----        ");
		combosearchRec.add("Voucher No");
		combosearchRec.add("Time Interval (From-To)");
		combosearchRec.add("Amount");
		combosearchRec.add("Narration");
		combosearchRec.select(0);
		
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(13);
		layout.right = new FormAttachment(30);
		//layout.bottom = new FormAttachment(10);
		combosearchRec.setLayoutData(layout);
		
		/*combosearchRec.clearSelection();
		combosearchRec.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
		combosearchRec.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		*/
//search voucher by voucher number		
		
		lblEntVoucherNo = new Label(this,SWT.NONE);
		lblEntVoucherNo.setText("&Voucher No : ");
		lblEntVoucherNo.setFont(new Font(display, "Time New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(39);
		layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(11);
		lblEntVoucherNo.setLayoutData(layout);
		lblEntVoucherNo.setVisible(false);
		
		txtEntVoucherNo = new Text(this,SWT.BORDER);
		txtEntVoucherNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(64);
		//layout.bottom = new FormAttachment(11);
		txtEntVoucherNo.setLayoutData(layout);
		txtEntVoucherNo.setVisible(false);
		
		
		
		//date fields when search voucher by time interval(From-to)
		lblFromDate = new Label(this,SWT.NONE);
		lblFromDate.setText("From &Date: ");
		lblFromDate.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(45);
		//layout.bottom = new FormAttachment(11);
		lblFromDate.setLayoutData(layout);
		lblFromDate.setVisible(false);
		
		txtFromddate = new Text(this,SWT.BORDER);
		txtFromddate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtFromddate.setTextLimit(2);
		txtFromddate.setMessage("dd");
		txtFromddate.setText(globals.session[2].toString().substring(0,2));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(46);
		layout.right = new FormAttachment(49);
		//layout.bottom = new FormAttachment(9);
		txtFromddate.selectAll();
		txtFromddate.setLayoutData(layout);
		txtFromddate.setVisible(false);
		
		lblFromDtDash1 = new Label(this, SWT.NONE);
		lblFromDtDash1.setText("-");
		lblFromDtDash1.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(49);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(9);
		lblFromDtDash1.setLayoutData(layout);
		lblFromDtDash1.setVisible(false);
		
		txtFromMdate = new Text(this,SWT.BORDER);
		txtFromMdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtFromMdate.setTextLimit(2);
		txtFromMdate.setMessage("mm");
		txtFromMdate.setText(globals.session[2].toString().substring(3,5));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(9);
		txtFromMdate.selectAll();
		txtFromMdate.setLayoutData(layout);		
		txtFromMdate.setVisible(false);
		
		lblFromDtDash2 = new Label(this, SWT.NONE);
		lblFromDtDash2.setText("-");
		lblFromDtDash2.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(53);
		layout.right = new FormAttachment(54);
		//layout.bottom = new FormAttachment(9);
		lblFromDtDash2.setLayoutData(layout);
		lblFromDtDash2.setVisible(false);
		
		txtFromYrdate = new Text(this,SWT.BORDER);
		txtFromYrdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtFromYrdate.setTextLimit(4);
		txtFromYrdate.setMessage("yyyy");
		txtFromYrdate.setText(globals.session[2].toString().substring(6));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(59);
		//layout.bottom = new FormAttachment(9);
		txtFromYrdate.setLayoutData(layout);
		txtFromYrdate.selectAll();
		txtFromYrdate.setVisible(false);
		
		lblToDate = new Label(this,SWT.NONE);
		lblToDate.setText("T&o Date :");
		lblToDate.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblFromDate,10);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(45);
		//layout.bottom = new FormAttachment(18);
		lblToDate.setLayoutData(layout);
		lblToDate.setVisible(false);
		
		txtToDdate = new Text(this,SWT.BORDER);
		txtToDdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtToDdate.setTextLimit(2);
		txtToDdate.setMessage("dd");
		txtToDdate.setText(globals.session[3].toString().substring(0,2));
		layout = new FormData();
		layout.top = new FormAttachment(lblFromDate,10);
		layout.left = new FormAttachment(46);
		layout.right = new FormAttachment(49);
		//layout.bottom = new FormAttachment(16);
		txtToDdate.selectAll();
		txtToDdate.setLayoutData(layout);
		txtToDdate.setVisible(false);
		
		lblToDateDash1 = new Label(this, SWT.NONE);
		lblToDateDash1.setText("-");
		lblToDateDash1.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblFromDate,10);
		layout.left = new FormAttachment(49);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(16);
		lblToDateDash1.setLayoutData(layout);
		lblToDateDash1.setVisible(false);
		
		txtToMdate = new Text(this,SWT.BORDER);
		txtToMdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtToMdate.setTextLimit(2);
		txtToMdate.setMessage("mm");
		txtToMdate.setText(globals.session[3].toString().substring(3,5));
		layout = new FormData();
		layout.top = new FormAttachment(lblFromDate,10);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(16);
		txtToMdate.selectAll();
		txtToMdate.setLayoutData(layout);
		txtToMdate.setVisible(false);
		
		lblToDateDash2 = new Label(this, SWT.NONE);
		lblToDateDash2.setText("-");
		lblToDateDash2.setFont(new Font(display, "Time New Roman",12,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblFromDate,10);
		layout.left = new FormAttachment(53);
		layout.right = new FormAttachment(54);
		//layout.bottom = new FormAttachment(16);
		lblToDateDash2.setLayoutData(layout);
		lblToDateDash2.setVisible(false);
		
		txtToYrdate = new Text(this,SWT.BORDER);
		txtToYrdate.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtToYrdate.setTextLimit(4);
		txtToYrdate.setMessage("yyyy");
		txtToYrdate.setText(globals.session[3].toString().substring(6));
		layout = new FormData();
		layout.top = new FormAttachment(lblFromDate,10);
		layout.left = new FormAttachment(54);
		layout.right = new FormAttachment(59);
		//layout.bottom = new FormAttachment(16);
		txtToYrdate.selectAll();
		txtToYrdate.setLayoutData(layout);
		txtToYrdate.setVisible(false);
		
// searching records by entering  Amount 
		lblentamount = new Label(this, SWT.NONE);
		lblentamount.setText("Amo&unt :");
		lblentamount.setFont(new Font(display, "Time New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(51);
		//layout.bottom = new FormAttachment(11);
		lblentamount.setLayoutData(layout);
		lblentamount.setVisible(false);
		
		txtentamount = new Text(this,SWT.RIGHT | SWT.BORDER);
		txtentamount.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		txtentamount.setMessage("0.00");
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(63);
		//layout.bottom = new FormAttachment(11);
		txtentamount.selectAll();
		txtentamount.setLayoutData(layout);
		txtentamount.setVisible(false);
		
// searching records by narration
		lblNarration = new Label(this, SWT.NONE);
		lblNarration.setText("Narrat&ion containing :");
		lblNarration.setFont(new Font(display, "Time New Roman",10,SWT.None));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(37);
		layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(11);
		lblNarration.setLayoutData(layout);
		lblNarration.setVisible(false);
		
		txtnarration = new Text(this,SWT.BORDER | SWT.MULTI | SWT.WRAP);
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,10);
		layout.left = new FormAttachment(56);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(lblLine,105);
		txtnarration.setLayoutData(layout);
		txtnarration.setVisible(false);
		
		btnsearch = new Button(this,SWT.PUSH );
		btnsearch.setText("&Search");
		btnsearch.setFont(new Font(display,"Time New Roman",10,SWT.BOLD ));
		layout = new FormData();
		layout.top= new FormAttachment(lblLine,10);
		layout.left= new FormAttachment(85);
		layout.right= new FormAttachment(92);
		//layout.bottom= new FormAttachment(12);
		btnsearch.setLayoutData(layout);
		btnsearch.setEnabled(false);
		
		
		projectlist=transactionController.getAllProjects();
		
		tblvoucherView = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION | SWT.LINE_SOLID );
		tblvoucherView.getTable().setFont(new Font(display, "UBUNTU",9,SWT.BOLD));
		tblvoucherView.getTable().setLinesVisible(true);
		tblvoucherView.getTable().setHeaderVisible(true);
		layout=new FormData();
		layout.top = new FormAttachment(btnsearch,50);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(90);
		tblvoucherView.getTable().setLayoutData(layout);
		
		
		/*btndelete = new Button(this,SWT.PUSH);
		btndelete.setText("&Delete");
		btndelete.setToolTipText("Select the vouchers you wish to delete by checking the checkboxes ");
		btndelete.setFont(new Font(display, "Time New Roman",13,SWT.NORMAL));
		layout= new FormData();
		layout.top = new FormAttachment(tblvoucherView.getTable(),2);
		layout.left = new FormAttachment(88);
		layout.right = new FormAttachment(98);
		//layout.bottom = new FormAttachment(96);
		btndelete.setLayoutData(layout);
		btndelete.setVisible(false);
*/
		this.getAccessible();
	    this.makeaccssible(this);
	    
		
	    sdf.setLenient(false);			
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		shellwidth = this.getClientArea().width;
		finshellwidth = shellwidth-(2*shellwidth/100);
	    //this.makeaccssible(grpVoucherResult);
	    //this.setLabelProvider();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
	    Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		lightBlue = new Color(this.getDisplay(),215,242,251);
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				combosearchRec.setFocus();
				combosearchRec.setBackground(FocusBackground);
				combosearchRec.setForeground(FocusForeground);
			}
		});
		

		globals.setThemeColor(this, Background, Foreground);
		tblvoucherView.getControl().setBackground(lightBlue);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        //globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		
		tabalternate =  new Color(this.getDisplay(),255, 51, 51);
		tabalternate1 =  new Color(this.getDisplay(),184, 255, 148);
		
		if (backFlag) {
			
			btnsearch.setEnabled(true);
			String fromDate = search_values[1];
			String toDate = search_values[2];
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			nf.setGroupingUsed(false);
			Double amount = 0.00;
			try {
				amount = Double.valueOf(nf.format(Double.valueOf(search_values[4])));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.getMessage();
			}
			//Composite grandParent = (Composite) tblextendedtrialbal.getTable().getParent().getParent();
			searchFlag = search_flag;
			searchValues = search_values;
			final ArrayList<VoucherDetail> vouchers1 = gnukhata.controllers.transactionController
					.searchVouchers(search_flag,search_values[0] , fromDate, toDate, search_values[3], amount);
			TableColumn[] cols = tblvoucherView.getTable().getColumns();
			try {
				for (int c = 0; c < cols.length; c++)
					tblvoucherView.getTable().getColumn(0).dispose();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tblvoucherView.remove(tblvoucherView.getTable().getItems());
			
			int CurrentTableWidth = tblvoucherView.getTable().getClientArea().width;
			colvNo = new TableViewerColumn(tblvoucherView , SWT.NONE);
			colvNo.getColumn().setWidth( 5 * CurrentTableWidth / 100);
			colvNo.getColumn().setText("V.No");
			colvNo.setLabelProvider(new ColumnLabelProvider()
			{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
			return vd.getVoucherNo();
			}
			
			}
			);
			
			TableViewerColumn colLock  = new TableViewerColumn(tblvoucherView, SWT.None);
			colLock.getColumn().setText("Status");
			colLock.getColumn().setAlignment(SWT.CENTER);
			colLock.getColumn().setWidth(4 * finshellwidth /100);
			colLock.setLabelProvider(new ColumnLabelProvider()
			{
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.VoucherDetail vno = (gnukhata.controllers.reportmodels.VoucherDetail) element;
				int lockflag;
				try {
					lockflag = transactionController.getLockFlag(Integer.parseInt(vno.getVoucherCode()));
					if (lockflag==1) {
						return "*";
					}
					else{
						return "";
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "";
				}
				
				}
			});
			
			
			
			colvType = new TableViewerColumn(tblvoucherView , SWT.NONE);
			colvType.getColumn().setWidth( 10 * CurrentTableWidth / 100);
			colvType.getColumn().setText("Type");
			colvType.setLabelProvider(new ColumnLabelProvider()
			{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
			return vd.getVoucherType();
			}
			
			}
			);
			
			coldateOfTran = new TableViewerColumn(tblvoucherView , SWT.NONE);
			coldateOfTran.getColumn().setWidth( 10 * CurrentTableWidth / 100);
			coldateOfTran.getColumn().setText("Date");
			coldateOfTran.setLabelProvider(new ColumnLabelProvider()
			{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
			return vd.getDateOfTransaction();
			}
			
			}
			);
			
			
			coldrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
			coldrAcc.getColumn().setWidth( 30 * CurrentTableWidth / 100);
			coldrAcc.getColumn().setText("Dr Account");
			coldrAcc.setLabelProvider(new ColumnLabelProvider()
			{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
			return vd.getDrAccount();
			}
			
			}
			);
			
			
			colcrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
			colcrAcc.getColumn().setWidth( 15 * CurrentTableWidth / 100);
			colcrAcc.getColumn().setText("             Cr Account");
			colcrAcc.setLabelProvider(new ColumnLabelProvider()
			{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
			return vd.getCrAccount();
			}
			
			}
			);
			
			
			colamount  = new TableViewerColumn(tblvoucherView , SWT.NONE);
			colamount.getColumn().setWidth( 8 * CurrentTableWidth / 100);
			colamount.getColumn().setText("Amount               ");
			colamount.getColumn().setAlignment(SWT.RIGHT);
			colamount.setLabelProvider(new ColumnLabelProvider()
			{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
			return vd.getAmount();
			}
			
			}
			);
			
			colprojName = new TableViewerColumn(tblvoucherView, SWT.None);
			colprojName.getColumn().setWidth(12*CurrentTableWidth/100);
			colprojName.getColumn().setText("               Project");
			colprojName.setLabelProvider(new ColumnLabelProvider(){
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getProjectName();
				}
			}
			);
			
			
			tblvoucherView.setContentProvider(new ArrayContentProvider());
			tblvoucherView.setInput(vouchers1);
			tblvoucherView.getTable().setFocus();
			tblvoucherView.getTable().setSelection(0);
			

		
			Display.getCurrent().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					tblvoucherView.getTable().setFocus();
					tblvoucherView.getTable().setSelection(0);
					TableItem[] items1 = tblvoucherView.getTable().getItems();
					for (int rowid=0; rowid<items1.length; rowid++){
					   
						if (vouchers1.get(rowid).getlockFlag()==1) 
					    {
					    	items1[rowid].setBackground(tabalternate);
					    }
					    else 
					    {
					    	items1[rowid].setBackground(tabalternate1);
					    }
					}
				}
				
			});
		}
		
		this.setEvents();
		

	}
	
	private void setEvents()
	{
		
		tblvoucherView.getTable().addFocusListener(new FocusAdapter() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				tblfocusindex = tblvoucherView.getTable().getSelectionIndex();
				tblvoucherView.getTable().setSelection(-1);
			}
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				tblvoucherView.getTable().setSelection(tblfocusindex);
			}
		});


		
		this.combosearchRec.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				btnsearch.setEnabled(false);
				String selectedSearchItem = combosearchRec.getItem(combosearchRec.getSelectionIndex());
				
				if(selectedSearchItem.equals("Voucher No"))
				{
					lblEntVoucherNo.setVisible(true);
					txtEntVoucherNo.setVisible(true);
					lblFromDate.setVisible(false);
					txtFromddate.setVisible(false);
					txtFromddate.setText("");
					lblFromDtDash1.setVisible(false);
					txtFromMdate.setVisible(false);
					txtFromMdate.setText("");
					lblFromDtDash2.setVisible(false);
					txtFromYrdate.setVisible(false);
					txtFromYrdate.setText("");
					lblToDate.setVisible(false);
					txtToDdate.setVisible(false);
					txtToDdate.setText("");
					lblToDateDash1.setVisible(false);
					txtToMdate.setVisible(false);
					txtToMdate.setText("");
					lblToDateDash2.setVisible(false);
					txtToYrdate.setVisible(false);
					txtToYrdate.setText("");
					lblentamount.setVisible(false);
					txtentamount.setVisible(false);
					txtentamount.setText("");
					lblNarration.setVisible(false);
					txtnarration.setVisible(false);
					txtnarration.setText("");
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				if(selectedSearchItem.equals("           ---- Please Select -----        "))
				{
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					btnsearch.setEnabled(false);
				}
				
				if(selectedSearchItem.equals("Time Interval (From-To)"))
				{
					lblFromDate.setVisible(true);
					txtFromddate.setVisible(true);
					lblFromDtDash1.setVisible(true);
					txtFromMdate.setVisible(true);
					lblFromDtDash2.setVisible(true);
					txtFromYrdate.setVisible(true);
					lblToDate.setVisible(true);
					txtToDdate.setVisible(true);
					lblToDateDash1.setVisible(true);
					txtToMdate.setVisible(true);
					lblToDateDash2.setVisible(true);
					txtToYrdate.setVisible(true);
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					lblentamount.setVisible(false);
					txtentamount.setVisible(false);
					lblNarration.setVisible(false);
					txtnarration.setVisible(false);
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				if(selectedSearchItem.equals("Amount"))
				{
					
					lblentamount.setVisible(true);
					txtentamount.setVisible(true);
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					lblFromDate.setVisible(false);
					txtFromddate.setVisible(false);
					txtFromddate.setText("");
					lblFromDtDash1.setVisible(false);
					txtFromMdate.setVisible(false);
					txtFromMdate.setText("");
					lblFromDtDash2.setVisible(false);
					txtFromYrdate.setVisible(false);
					txtFromYrdate.setText("");
					lblToDate.setVisible(false);
					txtToDdate.setVisible(false);
					txtToDdate.setText("");
					lblToDateDash1.setVisible(false);
					txtToMdate.setVisible(false);
					txtToMdate.setText("");
					lblToDateDash2.setVisible(false);
					txtToYrdate.setVisible(false);
					txtToYrdate.setText("");
					lblNarration.setVisible(false);
					txtnarration.setVisible(false);
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				
				if(selectedSearchItem.equals("Narration"))
				{
					lblNarration.setVisible(true);
					txtnarration.setVisible(true);
					lblFromDate.setVisible(false);
					txtFromddate.setVisible(false);
					txtFromddate.setText("");
					lblFromDtDash1.setVisible(false);
					txtFromMdate.setVisible(false);
					txtFromMdate.setText("");
					lblFromDtDash2.setVisible(false);
					txtFromYrdate.setVisible(false);
					txtFromYrdate.setText("");
					lblToDate.setVisible(false);
					txtToDdate.setVisible(false);
					txtToDdate.setText("");
					lblToDateDash1.setVisible(false);
					txtToMdate.setVisible(false);
					txtToMdate.setText("");
					lblToDateDash2.setVisible(false);
					txtToYrdate.setVisible(false);
					txtToYrdate.setText("");
					lblentamount.setVisible(false);
					txtentamount.setVisible(false);
					lblEntVoucherNo.setVisible(false);
					txtEntVoucherNo.setVisible(false);
					btnsearch.setEnabled(true);
					btnsearch.setVisible(true);
				}
				
			}
			
		});
		this.btnsearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{	
				ArrayList<VoucherDetail> vouchers = null;
				
				
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Voucher No"))
				{
					searchFlag = 1;
					btnsearch.setEnabled(true);
					
					String fromDate = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
					String toDate = globals.session[3].toString().substring(6) + "-" + globals.session[3].toString().substring(3,5) + "-" + globals.session[3].toString().substring(0,2);
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText().trim())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					//Composite grandParent = (Composite) tblextendedtrialbal.getTable().getParent().getParent();
					searchValues = new String[]{txtEntVoucherNo.getText().trim(), fromDate, toDate, txtnarration.getText().trim(),txtentamount.getText().trim()};
					vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, txtEntVoucherNo.getText().trim(), fromDate, toDate, txtnarration.getText().trim(),amount);
			TableColumn[] cols = tblvoucherView.getTable().getColumns();
					try {
						for(int c = 0; c < cols.length; c ++ )
							tblvoucherView.getTable().getColumn(0).dispose();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tblvoucherView.remove(tblvoucherView.getTable().getItems() );
					//tblvoucherView.refresh(true);
					int CurrentTableWidth = tblvoucherView.getTable().getClientArea().width;
					
					colvNo = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvNo.getColumn().setWidth( 5 * CurrentTableWidth / 100);
					colvNo.getColumn().setText("V.No");
					colvNo.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherNo();
					}
					
					}
					);
					
					TableViewerColumn colLock  = new TableViewerColumn(tblvoucherView, SWT.None);
					colLock.getColumn().setText("Status");
					colLock.getColumn().setAlignment(SWT.CENTER);
					colLock.getColumn().setWidth(4 * finshellwidth /100);
					colLock.setLabelProvider(new ColumnLabelProvider()
					{
						@Override
						public String getText(Object element) {
							// TODO Auto-generated method stub
						gnukhata.controllers.reportmodels.VoucherDetail vno = (gnukhata.controllers.reportmodels.VoucherDetail) element;
						int lockflag;
						try {
							lockflag = transactionController.getLockFlag(Integer.parseInt(vno.getVoucherCode()));
							if (lockflag==1) {
								return "*";
							}
							else{
								return "";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return "";
						}
						
						}
					});
					
					
					
					colvType = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvType.getColumn().setWidth( 10 * CurrentTableWidth / 100);
					colvType.getColumn().setText("Type");
					colvType.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherType();
					}
					
					}
					);
					
					coldateOfTran = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldateOfTran.getColumn().setWidth( 10 * CurrentTableWidth / 100);
					coldateOfTran.getColumn().setText("Date");
					coldateOfTran.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDateOfTransaction();
					}
					
					}
					);
					
					
					coldrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldrAcc.getColumn().setWidth( 30 * CurrentTableWidth / 100);
					coldrAcc.getColumn().setText("Dr Account");
					coldrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDrAccount();
					}
					
					}
					);
					
					
					colcrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colcrAcc.getColumn().setWidth( 15 * CurrentTableWidth / 100);
					colcrAcc.getColumn().setText("             Cr Account");
					colcrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getCrAccount();
					}
					
					}
					);
					
					
					colamount  = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colamount.getColumn().setWidth( 8 * CurrentTableWidth / 100);
					colamount.getColumn().setText("Amount               ");
					colamount.getColumn().setAlignment(SWT.RIGHT);
					colamount.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getAmount();
					}
					
					}
					);
					
					colprojName = new TableViewerColumn(tblvoucherView, SWT.None);
					colprojName.getColumn().setWidth(12*CurrentTableWidth/100);
					colprojName.getColumn().setText("               Project");
					colprojName.setLabelProvider(new ColumnLabelProvider(){
						@Override
						public String getText(Object element) {
							// TODO Auto-generated method stub
							gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
							return vd.getProjectName();
						}
					}
					);
					
					
					tblvoucherView.setContentProvider(new ArrayContentProvider());
					tblvoucherView.setInput(vouchers);
					tblvoucherView.getTable().setFocus();
					tblvoucherView.getTable().setSelection(0);
					

					//tblvoucherView.getTable().pack();
					
					
					 if(txtEntVoucherNo.getText().trim().equals("") )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("Please enter a valid VoucherNo.");
							msgDayErr.open();
							txtEntVoucherNo.setFocus();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtEntVoucherNo.setFocus();						
								}
							});
							return;
				
						}
					 if(vouchers.size() == 0)
						{
						
							MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
							msg.setText("Information!");
							msg.setMessage("There are no vouchers of this voucher number");
							msg.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									
									txtEntVoucherNo.setVisible(true);
									txtEntVoucherNo.setText("");
									txtEntVoucherNo.setFocus();
									
									
								}
					
								
							});
							return;
						
							//btnsearch.setEnabled(true);
							
						}
					
					
					
					
					
						
					
				}
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Time Interval (From-To)"))
				{
					if(txtFromddate.getText().trim().equals("")&&txtFromMdate.getText().trim().equals("")&&txtFromYrdate.getText().trim().equals("")&&txtToDdate.getText().trim().equals("")&&txtToMdate.getText().trim().equals("")&&txtToYrdate.getText().trim().equals("")||txtFromddate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Error!");
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtFromddate.setFocus();
						
						return;
					}
					
					if(txtFromMdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtFromMdate.setFocus();
						
						return;
					}
					
					
					if(txtFromYrdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtFromYrdate.setFocus();
						
						return;
					}
					
					if(txtToDdate.getText().trim().equals("")&&txtToMdate.getText().trim().equals("")&&txtToYrdate.getText().trim().equals("")||txtToDdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtToDdate.setFocus();
						
						return;
					}
					
					if(txtToMdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtToMdate.setFocus();
						
						return;
					}
					if(txtToYrdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a valid Date.");
						msgDayErr.open();
						txtToYrdate.setFocus();
						
						return;
					}
					try {
						
						Date OrgStart= sdf.parse(txtFromYrdate.getText() + "-" + txtFromMdate.getText() + "-" + txtFromddate.getText());
						Date OrgEnd = sdf.parse(txtToYrdate.getText() + "-" + txtToMdate.getText() + "-" + txtToDdate.getText());
						
						
						
						
						if(OrgStart.compareTo(OrgEnd)>0)
						{
							MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK | SWT.ICON_ERROR );
							msg.setText("Validation Date Error!");
							msg.setMessage("Please Enter Valid Period");
							txtFromddate.setText("");
							txtFromddate.setFocus();
							msg.open();
							
							
							return;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK | SWT.ICON_ERROR );
						msg.setText("Date Error!");
						msg.setMessage("Improper Date Format");
						txtFromddate.setFocus();
						msg.open();
						return;

					}
					
					searchFlag = 2;
					String fromDate = txtFromYrdate.getText() + "-" + txtFromMdate.getText() + "-" + txtFromddate.getText(); 
					String toDate = txtToYrdate.getText() + "-" + txtToMdate.getText() + "-" + txtToDdate.getText(); 
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText().trim())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					searchValues = new String[]{"", fromDate, toDate, txtnarration.getText().trim(),txtentamount.getText().trim()};
					vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, "", fromDate, toDate, txtnarration.getText(),amount);
					TableColumn[] cols = tblvoucherView.getTable().getColumns();
					try {
						for(int c = 0; c < cols.length; c ++ )
							tblvoucherView.getTable().getColumn(0).dispose();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tblvoucherView.remove(tblvoucherView.getTable().getItems() );
					//tblvoucherView.refresh(true);
					int CurrentTableWidth = tblvoucherView.getTable().getClientArea().width;
					colvNo = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvNo.getColumn().setWidth( 5 * CurrentTableWidth / 100);
					colvNo.getColumn().setText("V.No");
					colvNo.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherNo();
					}
					
					}
					);
					colvType = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvType.getColumn().setWidth( 14 * CurrentTableWidth / 100);
					colvType.getColumn().setText("                  Type");
					colvType.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherType();
					}
					
					}
					);
					
					coldateOfTran = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldateOfTran.getColumn().setWidth( 10 * CurrentTableWidth / 100);
					coldateOfTran.getColumn().setText("             Date");
					coldateOfTran.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDateOfTransaction();
					}
					
					}
					);
					
					
					coldrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldrAcc.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					coldrAcc.getColumn().setText("             Dr Account");
					coldrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDrAccount();
					}
					
					}
					);
					
					
					colcrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colcrAcc.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					colcrAcc.getColumn().setText("             Cr Account");
					colcrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getCrAccount();
					}
					
					}
					);
					
					
					colamount  = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colamount.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					colamount.getColumn().setText("Amount               ");
					colamount.getColumn().setAlignment(SWT.RIGHT);
					colamount.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getAmount();
					}
					
					}
					);
					
					colprojName = new TableViewerColumn(tblvoucherView, SWT.None);
					colprojName.getColumn().setWidth(12*CurrentTableWidth/100);
					colprojName.getColumn().setText("               Project");
					colprojName.setLabelProvider(new ColumnLabelProvider(){
						@Override
						public String getText(Object element) {
							// TODO Auto-generated method stub
							gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
							return vd.getProjectName();
						}
					}
					);	
					colnarr = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colnarr.getColumn().setWidth( 25 * CurrentTableWidth / 100);
					colnarr.getColumn().setText("                  Narration");
					colnarr.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getNarration();
					}
					
					}
					);

					tblvoucherView.setContentProvider(new ArrayContentProvider());
					tblvoucherView.setInput(vouchers);
					

				//	tblvoucherView.getTable().pack();
					
					
					
					tblvoucherView.getTable().setFocus();
					tblvoucherView.getTable().setSelection(0);
					
					
					
					
					
					
					if(vouchers.size()== 0)
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION );
						msg1.setText("Information!");
						msg1.setMessage("There are no vouchers within this date");
						msg1.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								
								txtFromddate.setFocus();
								
							}
						});
						
						return;
						
					}
					//btndelete.setVisible(true);
					//btnsearch.notifyListeners(SWT.Selection, new Event());
					//btnsearch.setFocus();
					
				
				}
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Narration"))
				{
					searchFlag = 3;
					String fromDate = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
					String toDate = globals.session[3].toString().substring(6) + "-" + globals.session[3].toString().substring(3,5) + "-" + globals.session[3].toString().substring(0,2);
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText().trim())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					searchValues = new String[]{"", fromDate, toDate, txtnarration.getText().trim(),txtentamount.getText().trim()};
					vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, "", fromDate, toDate, txtnarration.getText(),amount);
					TableColumn[] cols = tblvoucherView.getTable().getColumns();
					try {
						for(int c = 0; c < cols.length; c ++ )
							tblvoucherView.getTable().getColumn(0).dispose();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tblvoucherView.remove(tblvoucherView.getTable().getItems() );
					//tblvoucherView.refresh(true);
					int CurrentTableWidth = tblvoucherView.getTable().getClientArea().width;
					colvNo = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvNo.getColumn().setWidth( 5 * CurrentTableWidth / 100);
					colvNo.getColumn().setText("V.No");
					colvNo.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherNo();
					}
					
					}
					);
					colvType = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvType.getColumn().setWidth( 14 * CurrentTableWidth / 100);
					colvType.getColumn().setText("                  Type");
					colvType.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherType();
					}
					
					}
					);
					
					coldateOfTran = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldateOfTran.getColumn().setWidth( 10 * CurrentTableWidth / 100);
					coldateOfTran.getColumn().setText("             Date");
					coldateOfTran.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDateOfTransaction();
					}
					
					}
					);
					
					
					coldrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldrAcc.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					coldrAcc.getColumn().setText("             Dr Account");
					coldrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDrAccount();
					}
					
					}
					);
					
					
					colcrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colcrAcc.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					colcrAcc.getColumn().setText("             Cr Account");
					colcrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getCrAccount();
					}
					
					}
					);
					
					
					colamount  = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colamount.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					colamount.getColumn().setText("Amount               ");
					colamount.getColumn().setAlignment(SWT.RIGHT);
					colamount.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getAmount();
					}
					
					}
					);
					
					colprojName = new TableViewerColumn(tblvoucherView, SWT.None);
					colprojName.getColumn().setWidth(12*CurrentTableWidth/100);
					colprojName.getColumn().setText("               Project");
					colprojName.setLabelProvider(new ColumnLabelProvider(){
						@Override
						public String getText(Object element) {
							// TODO Auto-generated method stub
							gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
							return vd.getProjectName();
						}
					}
					);			
					
					colnarr = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colnarr.getColumn().setWidth( 25 * CurrentTableWidth / 100);
					colnarr.getColumn().setText("                  Narration");
					colnarr.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getNarration();
					}
					
					}
					);

					tblvoucherView.setContentProvider(new ArrayContentProvider());
					tblvoucherView.setInput(vouchers);
					

				//	tblvoucherView.getTable().pack();
					
					tblvoucherView.getTable().setFocus();
					tblvoucherView.getTable().setSelection(0);
					
					if(vouchers.size() == 0)
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION );
						msg.setText("Information!");
						msg.setMessage("There are no vouchers with this narration");
						msg.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								txtnarration.setVisible(true);
								txtnarration.setFocus();
								
							}
						});
						
						return;
					}
					//btndelete.setVisible(true);
									
					
				} 
			
				if(combosearchRec.getItem(combosearchRec.getSelectionIndex()).equals("Amount"))
				{
					searchFlag = 4;
					String fromDate = globals.session[2].toString().substring(6) + "-" + globals.session[2].toString().substring(3,5) + "-" + globals.session[2].toString().substring(0,2);
					String toDate = globals.session[3].toString().substring(6) + "-" + globals.session[3].toString().substring(3,5) + "-" + globals.session[3].toString().substring(0,2);
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);
					Double amount = 0.00;
					try {
							amount = Double.valueOf(nf.format(Double.valueOf(txtentamount.getText().trim())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.getMessage();
					}
					searchValues = new String[]{"", fromDate, toDate, "",txtentamount.getText().trim()};
					vouchers = gnukhata.controllers.transactionController.searchVouchers(searchFlag, "", fromDate, toDate, "",amount);
					TableColumn[] cols = tblvoucherView.getTable().getColumns();
					try {
						for(int c = 0; c < cols.length; c ++ )
							tblvoucherView.getTable().getColumn(0).dispose();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tblvoucherView.remove(tblvoucherView.getTable().getItems() );
					//tblvoucherView.refresh(true);
					int CurrentTableWidth = tblvoucherView.getTable().getClientArea().width;
					colvNo = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvNo.getColumn().setWidth( 5 * CurrentTableWidth / 100);
					colvNo.getColumn().setText("V.No");
					colvNo.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherNo();
					}
					
					}
					);
					colvType = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colvType.getColumn().setWidth( 14 * CurrentTableWidth / 100);
					colvType.getColumn().setText("                  Type");
					colvType.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getVoucherType();
					}
					
					}
					);
					
					coldateOfTran = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldateOfTran.getColumn().setWidth( 10 * CurrentTableWidth / 100);
					coldateOfTran.getColumn().setText("             Date");
					coldateOfTran.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDateOfTransaction();
					}
					
					}
					);
					
					
					coldrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					coldrAcc.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					coldrAcc.getColumn().setText("             Dr Account");
					coldrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getDrAccount();
					}
					
					}
					);
					
					
					colcrAcc = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colcrAcc.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					colcrAcc.getColumn().setText("             Cr Account");
					colcrAcc.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getCrAccount();
					}
					
					}
					);
					
					
					colamount  = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colamount.getColumn().setWidth( 12 * CurrentTableWidth / 100);
					colamount.getColumn().setText("Amount               ");
					colamount.getColumn().setAlignment(SWT.RIGHT);
					colamount.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getAmount();
					}
					
					}
					);
					
					colprojName = new TableViewerColumn(tblvoucherView, SWT.None);
					colprojName.getColumn().setWidth(12*CurrentTableWidth/100);
					colprojName.getColumn().setText("               Project");
					colprojName.setLabelProvider(new ColumnLabelProvider(){
						@Override
						public String getText(Object element) {
							// TODO Auto-generated method stub
							gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
							return vd.getProjectName();
						}
					}
					);
					
					colnarr = new TableViewerColumn(tblvoucherView , SWT.NONE);
					colnarr.getColumn().setWidth( 25 * CurrentTableWidth / 100);
					colnarr.getColumn().setText("                  Narration");
					colnarr.setLabelProvider(new ColumnLabelProvider()
					{
					@Override
					public String getText(Object element) {
						// TODO Auto-generated method stub
					gnukhata.controllers.reportmodels.VoucherDetail vd = (VoucherDetail) element;
					return vd.getNarration();
					}
					
					}
					);
					tblvoucherView.setContentProvider(new ArrayContentProvider());
					tblvoucherView.setInput(vouchers);
					
					//tblvoucherView.getTable().pack();
					
					tblvoucherView.getTable().setFocus();
					tblvoucherView.getTable().setSelection(0);

					
					try {
						if(txtentamount.getText().trim().equals("")||!txtentamount.getText().equals("") && Double.valueOf(txtentamount.getText()) == 0)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("Please enter a valid Amount.");
							msgDayErr.open();
							txtentamount.setFocus();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtentamount.setFocus();						
								}
							});
							return;
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK |SWT.ICON_WARNING );
						errMsg.setText("Error!");
						errMsg.setMessage("Please enter a valid Amount.");
						txtentamount.setFocus();
						/*tblvoucherView.getTable().setHeaderVisible(false);
						tblvoucherView.getTable().setLinesVisible(false);*/
						errMsg.open();
						return;
					}
					/*try {
						txtentamount.setText(nf.format(Double.parseDouble(txtentamount.getText())));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
					if(vouchers.size() == 0)
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION );
						msg.setText("Information!");
						msg.setMessage("There are no vouchers of given amount");
						msg.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								txtentamount.setVisible(true);
								txtentamount.setText("");
								txtentamount.setFocus();
								
							}
						});
						
						return;
					}
				
					//btndelete.setVisible(true);
					
					
				}
				
				btnsearch.setEnabled(false);	
				
				
				TableItem[] items1 = tblvoucherView.getTable().getItems();
				for (int rowid=0; rowid<items1.length; rowid++){
				   
					if (vouchers.get(rowid).getlockFlag()==1) 
				    {
				    	items1[rowid].setBackground(tabalternate);
				    }
				    else 
				    {
				    	items1[rowid].setBackground(tabalternate1);
				    }
				}
			
			}
			
			
		});
		
	/*btndelete.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			//call the deleteVoucher from the transactionController.
			int voucherCounter = 0;
			while( voucherCounter   < viewButton.size() )
			{
				if(ChkDelButton.get(voucherCounter).getSelection())
				{
					transactionController.deleteVoucher(Integer.parsLeInt(ChkDelButton.get(voucherCounter).getData("vouchercode").toString()));
					voucherNo.get(voucherCounter).dispose();
					voucherNo.remove(voucherCounter);
					voucherType.get(voucherCounter).dispose();
					voucherType.remove(voucherCounter);
					voucherDate.get(voucherCounter).dispose();
					voucherDate.remove(voucherCounter);
					voucherDrAccount.get(voucherCounter).dispose();
					voucherDrAccount.remove(voucherCounter);
					voucherCrAccount.get(voucherCounter).dispose();
					voucherCrAccount.remove(voucherCounter);
					voucherAmount.get(voucherCounter).dispose();
					voucherAmount.remove(voucherCounter);
					voucherProjectName.get(voucherCounter).dispose();
					voucherProjectName.remove(voucherCounter);
					voucherNarration.get(voucherCounter).dispose();
					voucherNarration.remove(voucherCounter);
					viewButton.get(voucherCounter).dispose();
					viewButton.remove(voucherCounter);
					ChkDelButton.get(voucherCounter).dispose();
					ChkDelButton.remove(voucherCounter);

				}
				else
				{
					voucherCounter ++;
				}
				//combosearchRec.setFocus();
			}
		}
	});	
		
	btndelete.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) 
		{
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.ARROW_UP )
			{
					if(viewButton.size() > 0)
					{
						viewButton.get(counter).setFocus();
					}
					if(viewButton.size()==0)
					{
						combosearchRec.setFocus();
					}
			}
		}
	});
*/		
	/*	combosearchRec.addFocusListener(new FocusAdapter() {
			 public void focusGained(FocusEvent arg0)
			 {
				combosearchRec.setBackground(FocusBackground);
					combosearchRec.setForeground(FocusForeground);

					//btndelete.setVisible(false); 
			 }
			 public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					combosearchRec.setBackground( Background);
					combosearchRec.setForeground( Foreground);
			 }
					
				
		
			});
*/		
/*		btnsearch.addFocusListener(new FocusAdapter() {
			 public void focusGained(FocusEvent arg0)
			 {
				 //btnsearch.clearSelection();
				 btnsearch.setBackground(FocusBackground);
					btnsearch.setForeground(FocusForeground);

					//btndelete.setVisible(false); 
			 }
			 public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				 	
					btnsearch.setBackground( Background);
					btnsearch.setForeground(Foreground);
			 }
					
				
		
			});

*/		
	combosearchRec.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) 
		{
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				if(combosearchRec.getSelectionIndex()==0)
				{
					MessageBox msg = new MessageBox(new Shell(), SWT.ERROR| SWT.OK | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Please Select Valid Option");
					msg.open();
					combosearchRec.setFocus();
				}
				if(combosearchRec.getSelectionIndex()==1)
				{
					txtEntVoucherNo.setFocus();
				}
				if(combosearchRec.getSelectionIndex()==2)
				{
					txtFromddate.setFocus();	
						
				}
				if(combosearchRec.getSelectionIndex()==3)
				{
					txtentamount.setFocus();
				}
				if(combosearchRec.getSelectionIndex()==4)
				{
					txtnarration.setFocus();
				}
			}
		
		}
	});
	txtEntVoucherNo.addFocusListener(new FocusAdapter() {
	 public void focusGained(FocusEvent arg0)
	 {
	/*	 txtEntVoucherNo.clearSelection();
			txtEntVoucherNo.setBackground(FocusBackground);
			txtEntVoucherNo.setForeground(FocusForeground);
	*/

			//btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
	 }
	/* public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			txtEntVoucherNo.setBackground(Background);
			txtEntVoucherNo.setForeground(Foreground);

		}
*/
	
	});
	
	txtEntVoucherNo.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				/*grpVoucherResult.setVisible(true);
				btndelete.setVisible(true);	
				
				//grpVoucherResult.setFocus();*/
				btnsearch.notifyListeners(SWT.Selection, new Event());
				
					//btnsearch.setFocus();
			
				
			
		
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{			
				txtEntVoucherNo.setText("");
				combosearchRec.setFocus();
			}
			}
	});
	txtentamount.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			if(verifyFlag== false)
			{
				arg0.doit= true;
				return;
			}
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
//first
		txtFromddate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				
					txtFromMdate.setFocus();
					
					
					
				
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{	
				combosearchRec.setFocus();
			}
		}
	});
	
	txtFromMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtFromMdate.getText().length()==txtFromMdate.getTextLimit())
			{
				txtFromMdate.setFocus();
			}
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				txtFromYrdate.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{	
				txtFromddate.setFocus();
			}
		}
	});
	
	txtFromYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtFromYrdate.getText().length()==txtFromYrdate.getTextLimit())
			{
				txtFromYrdate.setFocus();
			}
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				
					txtToDdate.setFocus();
				
				
			}
			
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromMdate.setFocus();
			}
		}
	});
	
	txtToDdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(txtToDdate.getText().length()==txtToDdate.getTextLimit())
			{
				txtToDdate.setFocus();
			}
			if(arg0.keyCode==SWT.CR |arg0.keyCode==SWT.KEYPAD_CR)
			{
				txtToMdate.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromYrdate.setFocus();
			}
		}
	});
	
	txtToMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtToMdate.getText().length()==txtToMdate.getTextLimit())
			{
				txtToMdate.setFocus();
			}
			if(arg0.keyCode==SWT.CR |arg0.keyCode==SWT.KEYPAD_CR)
			{
				txtToYrdate.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtToDdate.setFocus();
			}
		}
	});
	
	txtToDdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtToMdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtToYrdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtFromddate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			if(verifyFlag== false)
			{
				arg0.doit= true;
				return;
			}
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
txtFromMdate.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			if(verifyFlag== false)
			{
				arg0.doit= true;
				return;
			}
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
	txtFromYrdate.addVerifyListener(new VerifyListener() {
	
	@Override
	public void verifyText(VerifyEvent arg0) {
		// TODO Auto-generated method stub
		if(verifyFlag== false)
		{
			arg0.doit= true;
			return;
		}
		switch (arg0.keyCode) {
        case SWT.BS:           // Backspace
        case SWT.DEL:          // Delete
        case SWT.HOME:         // Home
        case SWT.END:          // End
        case SWT.ARROW_LEFT:   // Left arrow
        case SWT.ARROW_RIGHT:  // Right arrow
        case SWT.TAB:
        case SWT.CR:
        case SWT.KEYPAD_CR:
        case SWT.KEYPAD_DECIMAL:
            return;
    }
		if(arg0.keyCode==46)
		{
			return;
		}
    if (!Character.isDigit(arg0.character)) {
        arg0.doit = false;  // disallow the action
    }

	}
});
	txtToYrdate.addFocusListener(new FocusAdapter() {
		 public void focusGained(FocusEvent arg0)
		 {
			 //grpVoucherResult.setVisible(false);
				//btndelete.setVisible(false);
			/* txtToYrdate.clearSelection();
				txtToYrdate.setBackground(FocusBackground);
				txtToYrdate.setForeground(FocusForeground);
			*/
				btnsearch.setEnabled(true);
				btnsearch.setVisible(true);
						 }
		 /*public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
			txtToYrdate.setBackground(Background);
				txtToYrdate.setForeground(Foreground);

			}
*/
		});
	
	txtToYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(txtToYrdate.getText().length()==txtToYrdate.getTextLimit())
			{
				txtToYrdate.setFocus();
			}
			if(arg0.keyCode==SWT.TAB)
			{
				if(txtToYrdate.getText().trim().equals(""))
				{
					//txtToYrdate.setFocus();
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToYrdate.setFocus();					
						}
					});
					return;
						
				}
				if(!txtToYrdate.getText().trim().equals(""))
				{
					
					btnsearch.setEnabled(true);
					btnsearch.setFocus();
				}
			}
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{	
				/*grpVoucherResult.setVisible(true);
				btndelete.setVisible(true);
				btnsearch.notifyListeners(SWT.Selection, new Event());
				//btnsearch.setFocus();
				grpVoucherResult.setFocus();*/
				//btnsearch.notifyListeners(SWT.Selection, new Event());
				
					if(txtToYrdate.getText().trim().equals(""))
					{
						txtToYrdate.setFocus();
						
							
					}
					if(!txtToYrdate.getText().trim().equals(""))
					{
						//btnsearch.setEnabled(true);
						//btnsearch.setFocus();
						btnsearch.notifyListeners(SWT.Selection, new Event());
					}
					
				
				
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtToMdate.setFocus();
			}
		}
	});
	txtentamount.addFocusListener(new FocusAdapter() {
		 public void focusGained(FocusEvent arg0)
		 {
			 /*txtentamount.clearSelection();
				txtentamount.setBackground(FocusBackground);
				txtentamount.setForeground(FocusForeground);
			*/	
			 verifyFlag=true;
			 txtentamount.selectAll();
			 //grpVoucherResult.setVisible(false);
			 btnsearch.setEnabled(true);
				btnsearch.setVisible(true);
				//btndelete.setVisible(false); 
						 }
		 @Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			/* txtentamount.setBackground(Background);
				txtentamount.setForeground(Foreground);
			*/	
			super.focusLost(arg0);
			
			verifyFlag=false;
		}
		});
	txtentamount.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				//btnsearch.setFocus();
				btnsearch.notifyListeners(SWT.Selection, new Event());
			}
			
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				
				combosearchRec.setFocus();
				txtentamount.setText("");
			}
		}
	});
	
	txtnarration.addFocusListener(new FocusAdapter() {
		 public void focusGained(FocusEvent arg0)
		 {
				/*txtnarration.setBackground(FocusBackground);
				txtnarration.setForeground(FocusForeground);	
			*/
			// grpVoucherResult.setVisible(false);
			 btnsearch.setEnabled(true);
				btnsearch.setVisible(true);
				//btndelete.setVisible(false); 
				
						 }
	/*	 public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				txtnarration.setBackground( Background);
				txtnarration.setForeground( Foreground);
			}
	*/	 
		});
	txtnarration.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(arg0.keyCode==SWT.TAB)
			{
				arg0.doit=false;
				btnsearch.setFocus();
			}
			
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{/*
				grpVoucherResult.setVisible(true);
				btndelete.setVisible(true);
				btnsearch.notifyListeners(SWT.Selection, new Event());*/
				arg0.doit=false;
				btnsearch.notifyListeners(SWT.Selection, new Event());
				btnsearch.setEnabled(false);
				//grpVoucherResult.setFocus();
				
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtnarration.setText("");
				combosearchRec.setFocus();
			}
		}
	});
	
	btnsearch.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
			{
				btnsearch.setVisible(false);
			/*	grpVoucherResult.setEnabled(true);
				grpVoucherResult.setFocus();*/
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				if(txtnarration.isEnabled()==true)
				{
					txtnarration.setFocus();
				}
				if(txtentamount.isEnabled()==true)
				{
					txtentamount.setFocus();
				}
				if(txtToYrdate.isEnabled()==true)
				{
					txtToYrdate.setFocus();
				}
				if(txtEntVoucherNo.isEnabled()==true)
				{
					txtEntVoucherNo.setFocus();
				}
				
			}
		}
	});
	
	txtFromddate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtFromMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtFromYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	
	txtToDdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtToMdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	txtToYrdate.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if( (arg0.keyCode>= 65 && arg0.keyCode <= 90)||(arg0.keyCode>= 97 && arg0.keyCode <= 122) ||(arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
					arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
					arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
			{
				arg0.doit = true;
			}
			else
			{
				
				arg0.doit = false;
			}
		}
	});
	
	txtFromddate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			/*txtFromddate.setBackground(Background);
			txtFromddate.setForeground(Foreground);
			*/
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			verifyFlag=false;
			if(!txtFromddate.getText().trim().equals("") && Integer.valueOf ( txtFromddate.getText().trim())<10 && txtFromddate.getText().trim().length()< txtFromddate.getTextLimit())
			{
				txtFromddate.setText("0"+ txtFromddate.getText().trim());
				return;
			}
			if(!txtFromddate.getText().trim().equals("") && (Integer.valueOf(txtFromddate.getText().trim())> 31 || Integer.valueOf(txtFromddate.getText().trim()) <= 0) )
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msgdateErr.setText("Error!");
				msgdateErr.setMessage("You have entered an Invalid Date");
				msgdateErr.open();
				
				txtFromddate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtFromddate.setFocus();							
					}
				});
				return;
			}
			
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			/*txtFromddate.clearSelection();
			txtFromddate.setBackground(FocusBackground);
			txtFromddate.setForeground(FocusForeground);
			*/
			
			super.focusGained(arg0);
		//	grpVoucherResult.setVisible(false);
			//btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
			verifyFlag=true;
		}
	});
	
	
	txtFromYrdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			/*txtFromYrdate.setBackground(Background);
			txtFromYrdate.setForeground(Foreground);
			*/
			verifyFlag=false;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date voucherDate = sdf.parse(txtFromYrdate.getText() + "-" + txtFromMdate.getText() + "-" + txtFromddate.getText());
				Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
				Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
				
				if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
				{
					MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK | SWT.ICON_ERROR);
					errMsg.setText("Error!");
					errMsg.setMessage("Please enter the date within the financial year");
					errMsg.open();
					txtFromYrdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							
							txtFromYrdate.setFocus();
							
						}
					});
					
					return;
				}
			} 
			catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			/*txtFromYrdate.clearSelection();
			txtFromYrdate.setBackground(FocusBackground);
			txtFromYrdate.setForeground(FocusForeground);
	*/
			super.focusGained(arg0);
			verifyFlag=true;
		//	grpVoucherResult.setVisible(false);
			//btndelete.setVisible(false); 
			btnsearch.setVisible(true);
			btnsearch.setEnabled(true);
		}
	});
	
	txtFromMdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
	/*		txtFromMdate.setBackground(Background);
			txtFromMdate.setForeground(Foreground);
	*/		
			verifyFlag=false;
			if(! txtFromMdate.getText().trim().equals("") && Integer.valueOf ( txtFromMdate.getText().trim())<10 && txtFromMdate.getText().trim().length()< txtFromMdate.getTextLimit())
			{
				txtFromMdate.setText("0"+ txtFromMdate.getText().trim());
				return;
			}
			if(!txtFromMdate.getText().trim().equals("") && (Integer.valueOf(txtFromMdate.getText().trim())> 12 || Integer.valueOf(txtFromMdate.getText().trim()) <= 0))
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msgdateErr.setText("Validation Date Error!");
				msgdateErr.setMessage("You have entered an Invalid Month, please enter it in mm format.");
				msgdateErr.open();
				
				txtFromMdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						txtFromMdate.setFocus();
						
					}
				});
				return;
				
			}
			
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
	/*		txtFromMdate.clearSelection();
			txtFromMdate.setBackground(FocusBackground);
			txtFromMdate.setForeground(FocusForeground);
	*/
			super.focusGained(arg0);
			verifyFlag=true;
		//	grpVoucherResult.setVisible(false);
			//btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
		}
		
	});
	
		txtToDdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
	/*		txtToDdate.setBackground(Background);
			txtToDdate.setForeground(Foreground);
	*/		
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(!txtToDdate.getText().trim().equals("") && Integer.valueOf ( txtToDdate.getText().trim())<10 && txtToDdate.getText().trim().length()< txtToDdate.getTextLimit())
			{
				txtToDdate.setText("0"+ txtToDdate.getText().trim());
			}
			if(!txtToDdate.getText().trim().equals("") && (Integer.valueOf(txtToDdate.getText().trim())> 31 || Integer.valueOf(txtToDdate.getText().trim()) <= 0) )
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msgdateErr.setText("Validation Date Error!");
				msgdateErr.setMessage("You have entered an Invalid Date");
				msgdateErr.open();
				
				txtToDdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtToDdate.setFocus();
						
					}
				});
				
			}
			
		}
@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
	/*txtToDdate.clearSelection();
	txtToDdate.setBackground(FocusBackground);
	txtToDdate.setForeground(FocusForeground);
*/
		verifyFlag=true;
		btnsearch.setEnabled(true);
		btnsearch.setVisible(true);
		//grpVoucherResult.setVisible(false);
		//btndelete.setVisible(false); 
	}
	});
	
	txtToMdate.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			txtToMdate.setBackground(Background);
		txtToMdate.setForeground(Foreground);
		
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(!txtToMdate.getText().trim().equals("") && Integer.valueOf ( txtToMdate.getText().trim())<10 && txtToMdate.getText().trim().length()< txtToMdate.getTextLimit())
			{
				 txtToMdate.setText("0"+  txtToMdate.getText().trim());
			}
			if(!txtToMdate.getText().trim().equals("") && (Integer.valueOf(txtToMdate.getText().trim())> 12 || Integer.valueOf(txtToMdate.getText().trim()) <= 0) )
			{
				MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msgdateErr.setText("Validation Date Error!");
				msgdateErr.setMessage("You have entered an Invalid Month");
				msgdateErr.open();
				
				txtToMdate.setText("");
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtToMdate.setFocus();
						
					}
				});
				
			}
			
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
//			txtToMdate.clearSelection();
//			txtToMdate.setBackground(FocusBackground);
//			txtToMdate.setForeground(FocusForeground);
	
			verifyFlag=true;
		//	grpVoucherResult.setVisible(false);
		//	btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
		}
	});
	
	
	txtToYrdate.addFocusListener(new FocusAdapter() {
		
		@Override
		public void focusLost(FocusEvent arg0) {
/*			txtToYrdate.setBackground(Background);
			txtToYrdate.setForeground(Foreground);
*/			
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date ledgerStart = sdf.parse(txtFromYrdate.getText()+ "-"+ txtFromMdate.getText()+"-"+ txtFromddate.getText() );
				Date ledgerEnd = sdf.parse(txtToYrdate.getText()+ "-"+ txtToMdate.getText()+"-"+ txtToDdate.getText() );
				Date financialStart = sdf.parse(globals.session[2].toString().substring(6) +"-"+globals.session[2].toString().substring(3,5)+"-"+ globals.session[2].toString().substring(0,2));
				Date financialEnd = sdf.parse(globals.session[3].toString().substring(6) +"-"+globals.session[3].toString().substring(3,5)+"-"+ globals.session[3].toString().substring(0,2));
				if((ledgerStart.compareTo(financialStart)< 0 || ledgerStart.compareTo(financialEnd)>0)|| (ledgerEnd.compareTo(financialStart)<0 || ledgerEnd.compareTo(financialEnd)> 0 ) )
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK | SWT.ICON_ERROR);
					msg.setText("Validation Date Error!");
					msg.setMessage("Please enter the date range within the financial year");
		//			grpVoucherResult.setVisible(false);
					//btndelete.setVisible(false);
					msg.open();
					txtToYrdate.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtToYrdate.setFocus();
						}
					});
					
					return;
				}
								
			} catch(java.text.ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!txtToYrdate.getText().trim().equals(""))
			{
				btnsearch.setEnabled(true);
			}
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
/*			txtToYrdate.clearSelection();
			txtToYrdate.setBackground(FocusBackground);
			txtToYrdate.setForeground(FocusForeground);
*/	
			verifyFlag=true;
		//	grpVoucherResult.setVisible(false);
			//btndelete.setVisible(false); 
			btnsearch.setEnabled(true);
			btnsearch.setVisible(true);
		}
		
	});
	
	
/*	txtentamount.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			if(txtentamount.getText().trim().equals(""))
			{
				MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
				msgDayErr.setMessage("Please enter a valid amount.");
				msgDayErr.open();
				txtentamount.setFocus();
				display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtentamount.setFocus();						
					}
				});
				return;
			}
			
			try {
				txtentamount.setText(nf.format(Double.parseDouble(txtentamount.getText())));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		}
	
	});*/
	
	/*tblvoucherView.getControl().addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			tblvoucherView.getControl().setBackground(bgtblColor);
			tblvoucherView.getControl().setForeground(fgtblColor);
		};
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			tblvoucherView.getControl().setBackground(Display.getDefault().getSystemColor(SWT.COLOR_MAGENTA));
			tblvoucherView.getControl().setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));

			//focusflag = false;
		}
	});*/
		
	
tblvoucherView.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter(){
	
	@Override
	public void keyPressed(org.eclipse.swt.events.KeyEvent arg0) {
		// TODO Auto-generated method stub
		
		if (globals.session[7].toString().equals("-1") || globals.session[7].toString().equals("0")) {
			if (arg0.keyCode == SWT.SPACE) {
				//drilldown here, make a call to showLedger.
				int lockflag = 1;
				IStructuredSelection selection = (IStructuredSelection) tblvoucherView.getSelection();
				VoucherDetail vd = (VoucherDetail) selection.getFirstElement();
				System.out.println(vd.getlockFlag());
				try {
					if (vd.getVoucherCode().equals("")) {
						return;

					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return;
				}
				if (vd.getlockFlag() == 1) {
					lockflag = 0;
				} else {
					lockflag = 1;
				}

				int vouchercode = Integer.parseInt(vd.getVoucherCode());
				boolean lockstatus = transactionController.setLockFlag(vouchercode, lockflag);

				if (lockstatus) {
					vd.setLockflag(lockflag);

					if (lockflag == 1)
					{
						TableItem item = tblvoucherView.getTable().getItem(tblvoucherView.getTable().getSelectionIndex());
						item.setBackground(tabalternate);
					} 
					else {
						TableItem item = tblvoucherView.getTable().getItem(tblvoucherView.getTable().getSelectionIndex());
						item.setBackground(tabalternate1);

					}
					tblvoucherView.refresh();
				}
			}
		}
		if (arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR) {
			//drilldown here, make a call to showLedger.
			IStructuredSelection selection = (IStructuredSelection) tblvoucherView.getSelection();
			VoucherDetail vd = (VoucherDetail) selection.getFirstElement();
			try {
				try {
					if (vd.getVoucherCode().equals("")) {
						return;
						
					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return;
				}
				Composite grandParent = (Composite) tblvoucherView.getTable().getParent().getParent();
				System.out.println("this is theeeeeeeeeeee: "+grandParent.toString()+" one less: "+tblvoucherView.getTable().getParent().toString());
				int vouchercode = Integer.parseInt(vd.getVoucherCode());
				lock = vd.getLockflag();
				if(findvoucher==true)
				{
					try {
						transactionController.showVoucherDetail(grandParent,typeFlag, vouchercode,findvoucher,lock,searchFlag,searchValues);
						transactionController.getVoucherMaster(vouchercode);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					try {
						transactionController.showVoucherDetail(grandParent,typeFlag, vouchercode,false,lock, searchFlag, searchValues);
						transactionController.getVoucherMaster(vouchercode);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				tblvoucherView.getTable().getParent().dispose();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(arg0.keyCode == SWT.DEL)
		{
			IStructuredSelection selection = (IStructuredSelection) tblvoucherView.getSelection();
			VoucherDetail vd = (VoucherDetail) selection.getFirstElement();

			//MessageBox DeleteMessage = new MessageBox(new Shell(), SWT.YES| SWT.NO | SWT.ICON_QUESTION);
			//DeleteMessage.setText("Confirm?");
			CustomDialog DeleteMessage= new CustomDialog(new Shell());
			DeleteMessage.SetMessage("You are about to delete voucher Number"+ vd.getVoucherNo()+ " are you sure you wish to do this?" );
			int answer = DeleteMessage.open();
			if(answer == SWT.YES)
			{

				if(  transactionController.deleteVoucher(Integer.valueOf( vd.getVoucherCode())))
				{
					
					tblvoucherView.remove(vd);;
					if (tblvoucherView.getTable().getItemCount()>0) {
						tblvoucherView.getTable().setSelection(0);
					}
				}

			}

		}
	}
});
tblvoucherView.getControl().addMouseListener(new org.eclipse.swt.events.MouseAdapter() {
	@Override
	public void mouseDoubleClick(MouseEvent arg0) {
		// TODO Auto-generated method stub
		//drilldown here, make a call to showLedger.
		IStructuredSelection selection = (IStructuredSelection) tblvoucherView.getSelection();
		VoucherDetail vd = (VoucherDetail) selection.getFirstElement();
		try {
			try {
				if (vd.getVoucherCode().equals("")) {
					return;
					
				}
			} catch (NullPointerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return;
			}
			
			Composite grandParent = (Composite) tblvoucherView.getTable().getParent().getParent().getParent().getParent();
			
			
			int vouchercode = Integer.parseInt(vd.getVoucherCode());
			lock = vd.getLockflag();
			if(findvoucher==true)
			{
				try {
					transactionController.showVoucherDetail(grandParent,typeFlag, vouchercode,findvoucher,lock, searchFlag, searchValues);
					transactionController.getVoucherMaster(vouchercode);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				try {
					transactionController.showVoucherDetail(grandParent,typeFlag, vouchercode,false,lock, searchFlag, searchValues);
					transactionController.getVoucherMaster(vouchercode);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			tblvoucherView.getTable().getParent().getParent().getParent().dispose();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
});
	

	
}
	 public void makeaccssible(Control c)
		{
			c.getAccessible();
			
			
		}

}